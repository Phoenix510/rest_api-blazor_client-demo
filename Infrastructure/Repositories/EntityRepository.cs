using ApplicationCore.Interfaces;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories;

public sealed class EntityRepository<TEntity> : IRepository<TEntity>
    where TEntity : class
{
    private ApiContext Context;

    private DbSet<TEntity> Set => Context.Set<TEntity>();

    public EntityRepository(ApiContext dbContext)
    {
        Context = dbContext;
    }

    public IQueryable<TEntity> Query => Set;

    public TEntity Find(params object[] keys)
    {
        return Set.Find(keys);
    }

    public void Insert(TEntity entity)
    {
        var entry = Context.Entry(entity);
        if (entry.State == EntityState.Detached)
            Set.Add(entity);
    }

    public void Delete(TEntity entity)
    {
        var entry = Context.Entry(entity);
        if (entry.State == EntityState.Detached)
            Set.Attach(entity);
        Set.Remove(entity);
    }

    public void Update(TEntity entity)
    {
        var entry = Context.Entry(entity);
        if (entry.State == EntityState.Detached)
            Set.Attach(entity);
        entry.State = EntityState.Modified;
    }

    public async Task SaveChangesAsync()
    {
        await Context.SaveChangesAsync();
    }
}