using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data;

public class ApiContext : DbContext
{
    public DbSet<Message> Messages { get; set; }
    public DbSet<PublicMessage> PublicMessages { get; set; }
    public DbSet<User> Users { get; set; }
    public DbSet<UserSession> UserSessions { get; set; }
    public DbSet<DirectMessage> DirectMessages { get; set; }
    public DbSet<Conversation> Conversations { get; set; }
    public DbSet<Country> Countries { get; set; }
    public DbSet<Continent> Continents { get; set; }
    public DbSet<SessionGeolocation> SessionsGeolocations { get; set; }

    public ApiContext(DbContextOptions<ApiContext> options) : base(options)
    {
        Database.Migrate();
    }
}
