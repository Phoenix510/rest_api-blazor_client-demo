﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class Setmultipleprimarykeyinconversationuserdetailstable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ConversationUserDetails",
                table: "ConversationUserDetails");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ConversationUserDetails",
                table: "ConversationUserDetails",
                columns: new[] { "OwnerId", "ConversationId" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ConversationUserDetails",
                table: "ConversationUserDetails");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ConversationUserDetails",
                table: "ConversationUserDetails",
                column: "OwnerId");
        }
    }
}
