﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Options;
using Microsoft.Extensions.Options;
using System.Net.Http.Json;

namespace Infrastructure.Services
{

    public class IpdataService : IGeolocationService
    {
        private const string baseURL = "https://api.ipdata.co/";
        private readonly string apiKey;
        HttpClient httpClient;

        public IpdataService(IOptions<AppOptions> appOptions) 
        {
            httpClient = new HttpClient();
            apiKey = appOptions.Value.Geo_Api_Key;
        }

        public async Task<IpDetails?> GetIpDetails(string ip)
        {
            return await httpClient.GetFromJsonAsync<IpDetails>($"{baseURL}/{ip}?api-key={apiKey}&fields=ip,city,region,region_code,country_name,country_code,continent_name,continent_code");
        }
    }
}
