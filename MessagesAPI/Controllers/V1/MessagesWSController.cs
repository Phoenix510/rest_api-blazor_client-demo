using System.Net;
using System.Net.WebSockets;
using ApplicationCore.Extensions;
using ApplicationCore.Interfaces;
using ApplicationCore.Services;
using BlazorShared.Contracts.V1;
using Microsoft.AspNetCore.Mvc;

namespace MessagesAPI.Controllers.V1;

//[Authorize]
[ApiController]
public class MessagesWSController : ControllerBase
{
    private readonly IWebSocketClientsService webSocketClientsService;
    private readonly ITokenValidationService tokenValidationService;

    public MessagesWSController(IWebSocketClientsService webSocketClientsService,
        ITokenValidationService tokenValidationService)
    {
        this.webSocketClientsService = webSocketClientsService;
        this.tokenValidationService = tokenValidationService;
    }

    [ApiExplorerSettings(IgnoreApi = true)]
    [Route(ApiRoutes.MessagesWSController.EstablishConnection)]
    [ProducesResponseType(typeof(FailResponse<object>), (int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(FailResponse<object>), (int)HttpStatusCode.Unauthorized)]
    public async Task<ActionResult> EstablishConnection()
    {
        if (!HttpContext.WebSockets.IsWebSocketRequest)
            return BadRequest(ApiResponse<object>.FailResponse("Invalid protocol"));

        string currUsername = string.Empty;
        var token = HttpContext.Request.Query["token"];

        //Anonymous user - generate random connection Id
        if (string.IsNullOrEmpty(token))
            currUsername = Guid.NewGuid().ToString();
        else
        {
            var principal = tokenValidationService.GetClaimsPrincipalFromToken(token);
            // token was invalid, but there was token so no connection will be established like when
            // anonymous connection is indicated (empty token) from beginning
            if (principal is null)
                return BadRequest(ApiResponse<object>.FailResponse("Invalid token"));
            currUsername = principal.GetUsername();
        }

        // somehow even when token is valid, the username wasn't present
        if (currUsername is null)
            return Unauthorized(ApiResponse<object>.FailResponse("Username in token is not present."));

        using WebSocket ws = await HttpContext.WebSockets.AcceptWebSocketAsync();
        var errors = await webSocketClientsService.AddClientConnection(ws, currUsername);


        if (errors.Any())
            return BadRequest(ApiResponse<object>.FailResponse("Connection wasn't established", errors));

        return new EmptyResult();
    }
}