using System.Net;
using ApplicationCore.Entities;
using ApplicationCore.Extensions;
using ApplicationCore.Interfaces;
using ApplicationCore.Services;
using AutoMapper;
using BlazorShared.Contracts.V1;
using BlazorShared.Contracts.V1.DTO;
using Infrastructure.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MessagesAPI.Controllers.V1;

[Authorize]
[ApiController]
public class ConversationsController : ControllerBase
{
    private readonly ApiContext context;
    private readonly IConversationService conversationService;
    private readonly IRepository<User> userRepository;
    private readonly IUriService uriService;
    private readonly IMapper mapper;

    public ConversationsController(ApiContext context, IConversationService conversationService,
        IRepository<User> userRepository, IUriService uriService, IMapper mapper)
    {
        this.context = context;
        this.conversationService = conversationService;
        this.userRepository = userRepository;
        this.uriService = uriService;
        this.mapper = mapper;
    }
    
    [HttpPost(ApiRoutes.Conversation.CreateNewConversation)]
    [ProducesResponseType(typeof(ApiResponse<ConversationDTO>), (int)HttpStatusCode.Created)]
    [ProducesResponseType(typeof(FailResponse<object>), (int)HttpStatusCode.Unauthorized)]
    public async Task<ActionResult> CreateNewConversation(CreateConversationModel createConversationModel)
    {
        User? currUser = userRepository.GetByUsername(User.GetUsername());

        if (currUser is null)
            return Unauthorized(ApiResponse<object>.FailResponse("Username in token is invalid."));

        var result = conversationService.Insert(createConversationModel, currUser);

        if (result.errors.Any())
            return BadRequest(ApiResponse<IEnumerable<string>>.FailResponse("Conversation wasn't created", result.errors));
        
        await context.SaveChangesAsync();
        return StatusCode(201, ApiResponse<ConversationDTO>.SuccessResponse(
            mapper.Map<ConversationDTO>(result.conversation)));
    }

    [HttpPost(ApiRoutes.Conversation.GetLastConversations)]
    [ProducesResponseType(typeof(ApiResponse<IEnumerable<ConversationDTO>>), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(FailResponse<object>), (int)HttpStatusCode.Unauthorized)]
    public async Task<ActionResult> GetLastConversations()
    {
        User? currUser = userRepository.GetByUsername(User.GetUsername());

        if (currUser is null)
            return Unauthorized(ApiResponse<object>.FailResponse("Username in token is invalid."));

        var result = await conversationService.GetLastConversationsWithLastMessage(currUser.Id);

        await context.SaveChangesAsync();
        return Ok(ApiResponse<IEnumerable<ConversationDTO>>.SuccessResponse(
            mapper.Map<IEnumerable<ConversationDTO>>(result)));
    }
    
    [HttpGet(ApiRoutes.Conversation.GetConversationDetails)]
    [ProducesResponseType(typeof(ApiResponse<ConversationDTO>), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(FailResponse<object>), (int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(FailResponse<object>), (int)HttpStatusCode.NotFound)]
    public async Task<ActionResult> GetConversationDetails(long conversationId)
    {
        User? currUser = userRepository.GetByUsername(User.GetUsername());

        if (currUser is null)
            return Unauthorized(ApiResponse<object>.FailResponse("Username in token is invalid."));

        var result = await conversationService.GetConversationDetails(currUser.Id, conversationId);

        if(result is null)
            return NotFound(ApiResponse<object>.FailResponse("Conversation does not exist."));
        
        return Ok(ApiResponse<ConversationDTO>.SuccessResponse(
            mapper.Map<ConversationDTO>(result)));
    }

    [AllowAnonymous]
    [HttpGet(ApiRoutes.Conversation.GetMessagesFromConversation)]
    [ProducesResponseType(typeof(PagedApiResponse<DirectMessageDTO>), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<PagedApiResponse<PublicMessageDTO>>> GetMessagesFromConversation(long conversationId,
        [FromQuery] PageRequest pageRequest)
    {
        User? currUser = userRepository.GetByUsername(User.GetUsername());

        if (currUser is null)
            return Unauthorized(ApiResponse<object>.FailResponse("Username in token is invalid."));

        var messages = await conversationService.GetMessagesFromConversation
            (conversationId, currUser.Id, pageRequest.PageSize, pageRequest.PageNumber);
        var mappedMessages = mapper.Map<IEnumerable<DirectMessageDTO>>(messages);

        var nextPage =
            mappedMessages.Any()
                ? uriService.GetMessagesFromConversationUri(conversationId, new PageRequest(pageRequest.PageSize, pageRequest.PageNumber + 1))
                    .ToString()
                : null;

        var previousPage = pageRequest.PageNumber - 1 > 0
            ? uriService.GetMessagesFromConversationUri(conversationId, new PageRequest(pageRequest.PageSize, pageRequest.PageNumber + 1))
                .ToString()
            : null;

        return Ok(new PagedApiResponse<DirectMessageDTO>
            (mappedMessages, pageRequest.PageNumber, pageRequest.PageSize, nextPage, previousPage));
    }
}