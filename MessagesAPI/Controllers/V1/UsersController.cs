using System.Net;
using ApplicationCore.Entities;
using ApplicationCore.Extensions;
using ApplicationCore.Interfaces;
using AutoMapper;
using BlazorShared.Contracts.V1;
using BlazorShared.Contracts.V1.DTO;
using Infrastructure.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MessagesAPI.Controllers.V1;

[Authorize]
[ApiController]
public class UsersController : ControllerBase
{
    private readonly ApiContext context;
    private readonly ISingUpService singUpService;
    private readonly IUserService userService;
    private readonly IRepository<User> userRepository;
    private readonly IMapper mapper;

    public UsersController(ApiContext context, ISingUpService singUpService, IUserService userService, IRepository<User> userRepository, IMapper mapper)
    {
        this.context = context;
        this.singUpService = singUpService;
        this.userService = userService;
        this.userRepository = userRepository;
        this.mapper = mapper;
    }
    
    [HttpGet]
    [Route(ApiRoutes.Users.GetUserDetails)]
    [ProducesResponseType(typeof(ApiResponse<UserDTO>), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(FailResponse<object>), (int)HttpStatusCode.NotFound)]
    public IActionResult GetUserDetails()
    {
        User? user = userRepository.GetByUsername(User.GetUsername());

        if (user is not null)
            return Ok(ApiResponse<UserDTO>.SuccessResponse(mapper.Map<User, UserDTO>(user)));
        return NotFound(ApiResponse<UserDTO>.FailResponse("Username in token is invalid."));
    }
    
    [HttpPost]
    [Route(ApiRoutes.Users.GetUsersWhichUsernameStartsWith)]
    [ProducesResponseType(typeof(ApiResponse<IEnumerable<SearchByUsernameRecord>>), (int)HttpStatusCode.OK)]
    public IActionResult GetUsersWhichUsernameStartsWith(SerachUserByUsernameModel serachUserByUsernameModel)
    {
        var matches = userService.GetUsersStartingWith(serachUserByUsernameModel.Username);
        return Ok(ApiResponse<IEnumerable<SearchByUsernameRecord>>.SuccessResponse(mapper.Map<IEnumerable<SearchByUsernameRecord>>(matches)));
    }
    
    [HttpPost]
    [AllowAnonymous]
    [Route(ApiRoutes.Users.SingUp)]
    [ProducesResponseType(typeof(ApiResponse<string>), (int)HttpStatusCode.Created)]
    [ProducesResponseType(typeof(FailResponse<object>), (int)HttpStatusCode.BadRequest)]
    public async Task<IActionResult> SingUp(CreateUserModel createUserModel)
    {
        var result = singUpService.SingUp(createUserModel);
        if (result.errors.Any())
            return BadRequest(ApiResponse<IEnumerable<string>>.FailResponse(result.detail, result.errors));
        
        await context.SaveChangesAsync();
        return StatusCode(201, ApiResponse<string>.SuccessResponse(result.detail));
    }
}