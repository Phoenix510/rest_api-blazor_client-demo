﻿using ApplicationCore.Entities;
using ApplicationCore.Extensions;
using ApplicationCore.Interfaces;
using BlazorShared.Contracts.V1;
using BlazorShared.Contracts.V1.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace MessagesAPI.Controllers.V1
{
    [Authorize]
    [ApiController]
    public class UserSessionsController : ControllerBase
    {
        private readonly IUserSessionsService userSessionsService;
        private readonly IRepository<User> userRepository;

        public UserSessionsController(IUserSessionsService userSessionsService, IRepository<User> userRepository)
        {
            this.userSessionsService = userSessionsService;
            this.userRepository = userRepository;
        }

        [HttpGet(ApiRoutes.UserSessions.GetUserSessions)]
        [ProducesResponseType(typeof(PagedApiResponse<UserSessionDTO>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> GetUserSessions([FromQuery] PageRequest pageRequest)
        {
            IEnumerable<UserSessionDTO> sessions = await userSessionsService
                .GetSessionsFor(User.GetUsername(), pageRequest.PageSize, pageRequest.PageNumber);

            return Ok(new PagedApiResponse<UserSessionDTO>
            (sessions, pageRequest.PageNumber, pageRequest.PageSize, string.Empty, string.Empty));
        }

        [HttpGet(ApiRoutes.UserSessions.DeleteSession)]
        [ProducesResponseType(typeof(ApiResponse<object>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(FailResponse<object>), (int)HttpStatusCode.Unauthorized)]
        public async Task<ActionResult> DeleteSession(string refreshToken)
        {
            User? currUser = userRepository.GetByUsername(User.GetUsername());

            if (currUser is null)
                return Unauthorized(ApiResponse<object>.FailResponse("Invalid session"));

            var outcome = await userSessionsService.DeleteSession(refreshToken, currUser.Id);
            if (!outcome.deleted)
                return Unauthorized(ApiResponse<object>.FailResponse(outcome.error));

            return Ok(ApiResponse<object>.SuccessResponse());
        }
    }
}
