using System.Net;
using ApplicationCore.Entities;
using ApplicationCore.Extensions;
using ApplicationCore.Interfaces;
using AutoMapper;
using BlazorShared.Contracts.V1;
using BlazorShared.Contracts.V1.DTO;
using Infrastructure.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MessagesAPI.Controllers.V1;

[Authorize]
[ApiController]
public class DirectMessagesController : ControllerBase
{
    private readonly ApiContext context;
    private readonly IDirectMessagesService directMessagesService;
    private readonly IRepository<User> userRepository;
    private readonly IMapper mapper;

    public DirectMessagesController(ApiContext context, IDirectMessagesService directMessagesService,
        IRepository<User> userRepository, IMapper mapper)
    {
        this.context = context;
        this.directMessagesService = directMessagesService;
        this.userRepository = userRepository;
        this.mapper = mapper;
    }

    [HttpPost(ApiRoutes.DirectMessages.Insert)]
    [ProducesResponseType(typeof(ApiResponse<DirectMessageDTO>), (int)HttpStatusCode.Created)]
    [ProducesResponseType(typeof(FailResponse<object>), (int)HttpStatusCode.Unauthorized)]
    public async Task<ActionResult<ApiResponse<DirectMessageDTO>>> InsertMessage(CreateDirectMessageModel messageModel)
    {
        User? currUser = userRepository.GetByUsername(User.GetUsername());

        if (currUser is null)
            return Unauthorized(ApiResponse<object>.FailResponse("Username in token is invalid."));

        var result = await directMessagesService.Insert(messageModel, currUser);
        if (result.errors.Any())
            return BadRequest(ApiResponse<IEnumerable<string>>.FailResponse("Message wasn't added", result.errors));
        
        return StatusCode(201, ApiResponse<DirectMessageDTO>.SuccessResponse(result.message));
    }
}