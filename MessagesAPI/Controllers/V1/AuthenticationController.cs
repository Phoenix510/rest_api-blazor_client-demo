using System.Net;
using ApplicationCore.Interfaces;
using Azure.Core;
using BlazorShared.Contracts.V1;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MessagesAPI.Controllers.V1;

[Authorize]
[ApiController]
public class AuthenticationController : ControllerBase
{
    private readonly IAuthenticationService authenticationService;

    public AuthenticationController(IAuthenticationService authenticationService)
    {
        this.authenticationService = authenticationService;
    }
    
    [AllowAnonymous]
    [HttpPost(ApiRoutes.Authentication.Login)]
    [ProducesResponseType(typeof(ApiResponse<TokenModel>), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(FailResponse<object>), (int)HttpStatusCode.Unauthorized)]
    public async Task<ActionResult<TokenModel>> Login([FromBody] LoginModel loginModel)
    {
        var auth = await authenticationService.AuthUser(loginModel.Username, loginModel.Password, HttpContext.Connection.RemoteIpAddress);
        if (!auth.valid)
            return Unauthorized(ApiResponse<TokenModel>.FailResponse("Invalid credentials."));

        return Ok(ApiResponse<TokenModel>.SuccessResponse(auth.token));
    }

    [AllowAnonymous]
    [HttpGet(ApiRoutes.Authentication.RefreshSession)]
    [ProducesResponseType(typeof(ApiResponse<TokenModel>), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(FailResponse<object>), (int)HttpStatusCode.Unauthorized)]
    public async Task<ActionResult<TokenModel>> RefreshSession(string refreshToken)
    {
        var auth = await authenticationService.RefreshSession(refreshToken, HttpContext.Connection.RemoteIpAddress);
        if (!auth.valid)
            return Unauthorized(ApiResponse<TokenModel>.FailResponse("Refresh token is invalid or it's lifetime has passed."));

        return Ok(ApiResponse<TokenModel>.SuccessResponse(auth.token));
    }
    
}