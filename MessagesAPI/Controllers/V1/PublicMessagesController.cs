using System.Net;
using ApplicationCore.Entities;
using ApplicationCore.Extensions;
using ApplicationCore.Interfaces;
using AutoMapper;
using BlazorShared.Contracts.V1;
using BlazorShared.Contracts.V1.DTO;
using Infrastructure.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MessagesAPI.Controllers.V1;

[Authorize]
[ApiController]
public class PublicMessagesController : ControllerBase
{
    private readonly ApiContext context;
    private readonly IPublicMessagesService publicMessagesService;
    private readonly IRepository<User> userRepository;
    private readonly IUriService uriService;
    private readonly IMapper mapper;

    public PublicMessagesController(ApiContext context, IPublicMessagesService publicMessagesService, 
        IRepository<User> userRepository, IUriService uriService, IMapper mapper)
    {
        this.context = context;
        this.publicMessagesService = publicMessagesService;
        this.userRepository = userRepository;
        this.uriService = uriService;
        this.mapper = mapper;
    }
    
    [AllowAnonymous]
    [HttpGet(ApiRoutes.PublicMessages.GetAll)]
    [ProducesResponseType(typeof(PagedApiResponse<PublicMessageDTO>), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<PagedApiResponse<PublicMessageDTO>>> GetAllMessages([FromQuery]PageRequest pageRequest)
    {
        var messages = await publicMessagesService.GetAll(pageRequest.PageSize, pageRequest.PageNumber);
        var mappedMessages = mapper.Map<IEnumerable<PublicMessageDTO>>(messages);

        var nextPage =
            mappedMessages.Any() 
                ? uriService.GetAllPublicMessagesUri(new PageRequest( pageRequest.PageSize, pageRequest.PageNumber + 1)).ToString()
                : null;
        
        var previousPage = pageRequest.PageNumber -1 > 0  
                ? uriService.GetAllPublicMessagesUri(new PageRequest( pageRequest.PageSize, pageRequest.PageNumber + 1)).ToString()
                : null;
        
        return Ok(new PagedApiResponse<PublicMessageDTO>
            (mappedMessages, pageRequest.PageNumber, pageRequest.PageSize, nextPage, previousPage));
    }
    
    [HttpPost(ApiRoutes.PublicMessages.Insert)]
    [ProducesResponseType(typeof(ApiResponse<PublicMessageDTO>), (int)HttpStatusCode.Created)]
    [ProducesResponseType(typeof(FailResponse<object>), (int)HttpStatusCode.Unauthorized)]
    public async Task<ActionResult<ApiResponse<PublicMessageDTO>>> InsertMessage(CreatePublicMessageModel messageModel)
    {
        User? currUser = userRepository.GetByUsername(User.GetUsername());

        if (currUser is null)
            return Unauthorized(ApiResponse<object>.FailResponse("Username in token is invalid."));

        var result = await publicMessagesService.Insert(messageModel, currUser);
        if (result.errors.Any())
            return BadRequest(ApiResponse<IEnumerable<string>>.FailResponse("Message wasn't added", result.errors));
        
        return StatusCode(201,ApiResponse<PublicMessageDTO>.SuccessResponse(mapper.Map<PublicMessage, PublicMessageDTO>(result.message)));
    }
}