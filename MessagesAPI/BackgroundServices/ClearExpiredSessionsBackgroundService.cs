﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace MessagesAPI.BackgroundServices
{
    internal class ClearExpiredSessionsBackgroundService : IHostedService, IDisposable
    {
        private readonly IServiceProvider services;
        private readonly ILogger<ClearExpiredSessionsBackgroundService> logger;
        private Timer? timer = null;

        public ClearExpiredSessionsBackgroundService(IServiceProvider services, 
            ILogger<ClearExpiredSessionsBackgroundService> logger)
        {
            this.services = services;
            this.logger = logger;
        }

        public void Dispose()
        {
            timer?.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("Clearing expired session service started.");

            timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromMinutes(30));

            return Task.CompletedTask;
        }

        private async void DoWork(object? state)
        {
            logger.LogInformation(
                "Clearing expired sessions");

            using (var scope = services.CreateScope())
            {
                var userSessionsService =
                    scope.ServiceProvider
                        .GetRequiredService<IUserSessionsService>();

                await userSessionsService.ClearExpiredSessions();
            }     
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("Clearing expired session service is stopping.");

            timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
    }
}
