using ApplicationCore.Interfaces;
using ApplicationCore.Services;

namespace MessagesAPI.ServiceInstallers;

public static class ServicesInstaller
{
    public static void AddServices(this IServiceCollection services)
        => services.AddTransient<ISingUpService, SingUpService>()
            .AddSingleton<IWebSocketClientsService, WebSocketClientsService>()
            .AddTransient<IAuthenticationService, AuthenticationService>()
            .AddTransient<IDirectMessagesService, DirectMessagesService>()
            .AddTransient<ITokenValidationService, TokenValidationService>()
            .AddTransient<IPublicMessagesService, PublicMessagesService>()
            .AddTransient<IConversationService, ConversationService>()
            .AddTransient<IUserService, UserService>()
            .AddTransient<IUserSessionsService, UserSessionsService>()
            .AddTransient<IIPDetailsResolver, IPDetailsResolver>()
            .AddHttpContextAccessor()
            .AddScoped<IUriService, UriService>(serviceProvider =>
            {
                var request = serviceProvider.GetRequiredService<IHttpContextAccessor>().HttpContext.Request;
                return new UriService(string.Concat(request.Scheme, "://", request.Host.ToUriComponent(), "/"));
            });
}