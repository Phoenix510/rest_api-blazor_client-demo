using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Infrastructure.Repositories;

namespace MessagesAPI.ServiceInstallers;

public static class RepositoriesInstaller
{
    public static void AddRepositories(this IServiceCollection services)
        => services.AddTransient<IRepository<Message>, EntityRepository<Message>>()
            .AddTransient<IRepository<PublicMessage>, EntityRepository<PublicMessage>>()
            .AddTransient<IRepository<DirectMessage>, EntityRepository<DirectMessage>>()
            .AddTransient<IRepository<Conversation>, EntityRepository<Conversation>>()
            .AddTransient<IRepository<UserSession> , EntityRepository<UserSession>>()
            .AddTransient<IRepository<Country>, EntityRepository<Country>>()
            .AddTransient<IRepository<Continent>, EntityRepository<Continent>>()
            .AddTransient<IRepository<User>, EntityRepository<User>>();
}