using ApplicationCore.JWT;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace MessagesAPI.ServiceInstallers;

public static class JwtInstaller
{
    public static void AddJwtAuthentication(this IServiceCollection services, JWTSettings jWTSettings)
     => services.AddAuthentication(x =>
         {
             x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
             x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
             x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
         }
     ).AddJwtBearer(x =>
     {
         x.TokenValidationParameters = jWTSettings.GetTokenValidationParameters();
     });
}