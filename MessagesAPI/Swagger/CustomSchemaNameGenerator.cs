namespace MessagesAPI.Swagger;

//https://blog.devgenius.io/nswag-csharp-client-with-generics-support-6ad6a09f81d6
public class CustomSchemaNameGenerator
{
    public string Generate(Type type)
    {
        return ConstructSchemaId(type);
    }

    private string ConstructSchemaId(Type type)
    {
        var typeName = type.Name;
        if (type.IsGenericType)
        {
            var genericArgs = string.Join(", ", type.GetGenericArguments().Select(ConstructSchemaId));

            int index = typeName.IndexOf('`');
            var typeNameWithoutGenericArity = index == -1 ? typeName : typeName.Substring(0, index);

            return $"{typeNameWithoutGenericArity}<{genericArgs}>";
        }
        return typeName;
    }
}