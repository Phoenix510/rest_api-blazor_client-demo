using System.IdentityModel.Tokens.Jwt;
using ApplicationCore.Interfaces;
using ApplicationCore.JWT;
using ApplicationCore.Mapping;
using ApplicationCore.Options;
using ApplicationCore.Services;
using BlazorShared.Converters;
using Infrastructure.Data;
using Infrastructure.Services;
using MessagesAPI.BackgroundServices;
using MessagesAPI.ServiceInstallers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

var builder = WebApplication.CreateBuilder(args);
IConfiguration config = builder.Configuration;

builder.Services.AddOptions<AppOptions>()
    .Bind(builder.Configuration.GetSection(AppOptions.Name))
    .ValidateDataAnnotations()
    .ValidateOnStart();

builder.Services.AddOptions<JwtOptions>()
    .Bind(builder.Configuration.GetSection(JwtOptions.Name))
    .ValidateDataAnnotations()
    .ValidateOnStart();


builder.Services.AddCors(options =>
{
    {
        var appOptions = builder.Services.BuildServiceProvider().GetService<IOptions<AppOptions>>()?.Value ?? 
                            throw new ArgumentNullException("No app settings configured");
        options.AddDefaultPolicy(
            policy =>
            {
                policy.WithOrigins(appOptions.Allowed_URIs).AllowAnyHeader().WithMethods("GET", "POST", "PUT", "DELETE");
            });
    }
});
builder.Services.AddApiVersioning(c =>
{
    c.DefaultApiVersion = new ApiVersion(1, 0);
    c.AssumeDefaultVersionWhenUnspecified = true;
    c.ReportApiVersions = true;
});

// Add services to the container.
AddServices(builder.Services);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

app.UseCors();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

var webSocketOptions = new WebSocketOptions
{
    KeepAliveInterval = TimeSpan.FromMinutes(2)
};
app.UseWebSockets(webSocketOptions);

app.Run();

void AddServices(IServiceCollection services)
{
    // the order matters
    services.AddTransient<JWTSettings>();
    services.AddJwtAuthentication(services.BuildServiceProvider().GetService<JWTSettings>() ?? throw new ArgumentNullException("No JWTSettings configured"));
    services.AddAuthorization();

    services.AddControllers();
    services.AddEndpointsApiExplorer();

    services.AddSwagger();

    services.AddDbContext<ApiContext>
        (options => options.UseSqlServer(Environment.GetEnvironmentVariable("ConnectionString")));
    services.AddRepositories();

    services.AddTransient<IJWTManager, JWTManager>();
    services.AddTransient<IGeolocationService, IpdataService>();
    services.AddTransient<IIPConverter, IPConverter>();
    services.AddServices();
    services.AddAutoMapper(typeof(MappingProfiles));
    services.AddHostedService<ClearExpiredSessionsBackgroundService>();
}