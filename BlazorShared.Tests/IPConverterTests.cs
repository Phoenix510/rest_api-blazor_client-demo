using BlazorShared.Converters;
using System.Net;

namespace BlazorShared.Tests
{
    public class IPConverterTests
    {
        [InlineData("127.0.0.1", 2130706433)]
        [InlineData("192.168.1.1", 3232235777)]
        [InlineData("19.117.63.126", 326451070)]
        [InlineData("255.255.255.255", 4294967295)]
        [Theory]
        public void Can_convert_ipv4_value_to_int_correctly(string IPv4AsString, uint expectedConvertionResult)
        {
            var sut = new IPConverter();

            var result = sut.GetIntFromIP(IPAddress.Parse(IPv4AsString));

            Assert.Equal(expectedConvertionResult, result);
        }

        [InlineData(2130706433, "127.0.0.1")]
        [InlineData(3232235777, "192.168.1.1")]
        [InlineData(326451070, "19.117.63.126")]
        [InlineData(4294967295, "255.255.255.255")]
        [Theory]
        public void Can_convert_int_value_to_ipv4_correctly(uint ipAsInt, string expectedConvertionResult)
        {
            var sut = new IPConverter();

            var result = sut.GetIPFromInt(ipAsInt);

            Assert.Equal(expectedConvertionResult, result.ToString());
        }
    }
}