using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces;

public interface IUserService
{
    IEnumerable<User> GetUsersStartingWith(string usernamePart, int count = 10);
}