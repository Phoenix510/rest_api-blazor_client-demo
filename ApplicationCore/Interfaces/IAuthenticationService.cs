using System.Net;
using System.Security.Claims;
using BlazorShared.Contracts.V1;

namespace ApplicationCore.Interfaces;

public interface IAuthenticationService
{
    Task<(bool valid, TokenModel token)> AuthUser(string username, string password, IPAddress ip);
    Task<(bool valid, TokenModel token)> RefreshSession(string refreshToken, IPAddress ip);
}