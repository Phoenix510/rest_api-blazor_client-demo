using ApplicationCore.Entities;
using BlazorShared.Contracts.V1;

namespace ApplicationCore.Interfaces;

public interface IPublicMessagesService
{
    Task<IEnumerable<PublicMessage>> GetAll(int pageSize, int pageNumber);

    Task<(IEnumerable<string> errors, PublicMessage message)> Insert(CreatePublicMessageModel createPublicMessageModel, User creator); 
}