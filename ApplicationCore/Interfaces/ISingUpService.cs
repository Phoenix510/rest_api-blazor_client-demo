using BlazorShared.Contracts.V1;

namespace ApplicationCore.Interfaces;

public interface ISingUpService
{
    (string detail, IEnumerable<string> errors) SingUp(CreateUserModel user);
}