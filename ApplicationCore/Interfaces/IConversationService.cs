using ApplicationCore.Entities;
using BlazorShared.Contracts.V1.DTO;

namespace ApplicationCore.Services;

public interface IConversationService
{
    Task<IEnumerable<DirectMessage>> GetMessagesFromConversation(long conversationId, long userId, int pageSize,
        int pageNumber);

    Task<IEnumerable<Conversation>> GetLastConversationsWithLastMessage(long userId, int numOfConversations = 10);

    (IEnumerable<string> errors, Conversation conversation) Insert(CreateConversationModel createConversationModel,
        User creator);

    Task<Conversation?> GetConversationDetails(long userId, long conversationId);
}