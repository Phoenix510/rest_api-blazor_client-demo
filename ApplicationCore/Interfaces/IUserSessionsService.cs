﻿using BlazorShared.Contracts.V1.DTO;

namespace ApplicationCore.Interfaces
{
    public interface IUserSessionsService
    {
        Task ClearExpiredSessions();
        Task<IEnumerable<UserSessionDTO>> GetSessionsFor(string username, int pageSize, int pageNumber);
        Task<(bool deleted, string error)> DeleteSession(string refreshToken, long userId);
    }
}