using System.Security.Claims;

namespace ApplicationCore.Interfaces;

public interface ITokenValidationService
{
    ClaimsPrincipal? GetClaimsPrincipalFromToken(string token);
}