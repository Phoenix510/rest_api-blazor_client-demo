using BlazorShared.Contracts.V1.DTO;

namespace ApplicationCore.Interfaces;

public interface IUriService
{
    Uri GetAllPublicMessagesUri(PageRequest pageRequest);
    Uri GetMessagesFromConversationUri(long conversationId, PageRequest pageRequest);
}