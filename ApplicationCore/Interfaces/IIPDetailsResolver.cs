﻿using ApplicationCore.Entities;
using System.Net;

namespace ApplicationCore.Interfaces
{
    public interface IIPDetailsResolver
    {
        Task<IpDetails?> GetIPDetails(IPAddress ip);
    }
}