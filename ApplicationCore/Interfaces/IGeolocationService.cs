﻿using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces
{
    public interface IGeolocationService
    {
        Task<IpDetails?> GetIpDetails(string ip);
    }
}
