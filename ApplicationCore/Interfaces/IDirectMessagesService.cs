using ApplicationCore.Entities;
using BlazorShared.Contracts.V1;

namespace ApplicationCore.Interfaces;

public interface IDirectMessagesService
{
    Task<(IEnumerable<string> errors, DirectMessageDTO message)>  Insert(CreateDirectMessageModel createDirectMessageModel,
        User creator);
}