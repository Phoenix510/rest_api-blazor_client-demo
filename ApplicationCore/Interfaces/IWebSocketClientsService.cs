using System.Net.WebSockets;

namespace ApplicationCore.Services;

public interface IWebSocketClientsService
{
    Task<IEnumerable<string>> AddClientConnection(WebSocket client, string ownerUsername);
    Task Send(string message, string recipientUsername = "ALL");
    void RefreshLastActivityTime(string username);
    void RemoveClientConnection(string ownerUsername);
}