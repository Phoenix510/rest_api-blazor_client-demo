﻿namespace ApplicationCore.JWT;

public interface IJWTManager
{
    public const string ACCESS_TOKEN = "AccessToken";
    public const string REFRESH_TOKEN = "RefreshToken";
    public const byte ACCESS_TOKEN_LIFETIME_IN_MINUTES = 15;
    public const byte REFRESH_TOKEN_LIFETIME_IN_MINUTES = 20;

    string GenerateAccessToken(string username);
    string GenerateRefreshToken(string username);
}
