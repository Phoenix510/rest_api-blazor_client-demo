﻿using ApplicationCore.Options;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace ApplicationCore.JWT
{
    public class JWTSettings
    {
        private readonly string issuer;
        private readonly string audience;
        private readonly string key;

        public JWTSettings(IOptions<JwtOptions> jwtOptions)
        {
            var options = jwtOptions.Value;
            issuer = options.Issuer;
            audience = options.Audience;
            key = options.Key;
        }

        public TokenValidationParameters GetTokenValidationParameters()
        => new()
        {
            ValidIssuer = issuer,
            ValidAudience = audience,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key)),
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            ClockSkew = TimeSpan.Zero
        };

    }

}

