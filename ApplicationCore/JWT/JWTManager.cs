using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using ApplicationCore.Options;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace ApplicationCore.JWT;

public class JWTManager: IJWTManager
{
    public const string ACCESS_TOKEN = "AccessToken";
    public const string REFRESH_TOKEN = "RefreshToken";
    public const string DEVICE_COOKIE = "DeviceCookie";
    public const byte ACCESS_TOKEN_LIFETIME_IN_MINUTES = 15;
    public const byte REFRESH_TOKEN_LIFETIME_IN_MINUTES = 20;

    private readonly string issuer;
    private string audience;
    private readonly string key;

    public JWTManager(IOptions<JwtOptions> jwtOptions)
    {
        var options = jwtOptions.Value;
        issuer = options.Issuer;
        audience = options.Audience;
        key = options.Key;
    }

    public string GenerateAccessToken(string username)
    {
        Claim[] claims = new[] {
            new Claim(ClaimTypes.Name, username),
           // new Claim(JwtRegisteredClaimNames.Sub, username),
            new Claim("type", ACCESS_TOKEN),
            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
        };

        return GenerateJWT(claims, DateTime.Now.AddMinutes(ACCESS_TOKEN_LIFETIME_IN_MINUTES));
    }

    public string GenerateRefreshToken(string username)
    {
        Claim[] claims = new[] {
            new Claim(ClaimTypes.Name, username),
            //new Claim(JwtRegisteredClaimNames.Sub, username),
            new Claim("type", REFRESH_TOKEN),
            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
        };

        audience = "RefreshToken";
        return GenerateJWT(claims, DateTime.Now.AddMinutes(REFRESH_TOKEN_LIFETIME_IN_MINUTES));
    }

    private string GenerateJWT(Claim[] claims, DateTime expires)
    {
        var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
        var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);

        var token = new JwtSecurityToken(issuer,
            audience,
            claims,
            expires: expires,
            signingCredentials: credentials);


        return new JwtSecurityTokenHandler().WriteToken(token);
    }
}