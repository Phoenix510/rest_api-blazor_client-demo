using ApplicationCore.Entities;
using ApplicationCore.Interfaces;

namespace ApplicationCore.Extensions
{
    public static class UserRepositoryExtension
    {
        public static User GetByUsername(this IRepository<User> repository, string username) =>
            repository.Query.FirstOrDefault(x => x.Username == username)!;

        public static User GetByEmail(this IRepository<User> repository, string email) =>
            repository.Query.FirstOrDefault(x => string.Equals(x.Email, email))!;
    }
}