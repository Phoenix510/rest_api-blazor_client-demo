using System.Security.Claims;

namespace ApplicationCore.Extensions;

public static class ClassPrinciplesExtensions
{
    public static string GetUsername(this ClaimsPrincipal claims) =>
        claims.FindFirst(ClaimTypes.Name)?.Value ?? string.Empty;
}