
namespace ApplicationCore.Entities;

public class Conversation
{
    public long Id { get; set; }
    public ICollection<User> Participants { get; set; }
    public ICollection<DirectMessage> Messages { get; set; }
    public ICollection<ConversationUserDetails> ConversationUserDetailsCollection { get; set; }

    public DateTimeOffset LastModification { get; set; }
}