using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace ApplicationCore.Entities;

[Index(propertyNames: nameof(Username), IsUnique = true)]
public class User
{
    public long Id { get; set; }
    [Required]
    public string Username { get; set; }
    [Required]
    public string Password { get; set; }
    [Required]
    public string Email { get; set; }
    
    public ICollection<Conversation> Conversations { get; set; }
    public ICollection<UserSession> Sessions { get; set; }
}