using System.ComponentModel.DataAnnotations;

namespace ApplicationCore.Entities;

public class DirectMessage: Message
{
    public bool Received { get; set; }
    public bool Read { get; set; }
    
    [Required]
    public long ConversationId { get; set; } 
    public Conversation Conversation { get; set; } = null!;
}