using System.ComponentModel.DataAnnotations;

namespace ApplicationCore.Entities;

public class Message
{
    public long Id { get; set; }
    [Required]
    [MaxLength(320)]
    public string Content { get; set; }
    [Required]
    public DateTimeOffset CreationDate { get; set; }
    [Required]
    public User Creator { get; set; }
}