﻿using System.ComponentModel.DataAnnotations;

namespace ApplicationCore.Entities
{
    public class Continent
    {
        [Key]
        [Required]
        [MaxLength(2)]
        public string Code { get; set; }
        [Required]
        [MaxLength(20)]
        public string Name { get; set; }
    }
}
