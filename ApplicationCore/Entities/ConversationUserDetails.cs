using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ApplicationCore.Entities;

[PrimaryKey(nameof(OwnerId), nameof(ConversationId))]
public class ConversationUserDetails
{
    [ForeignKey(nameof(Owner))]
    public long OwnerId { get; set; }
    public User Owner { get; set; }
    [ForeignKey(nameof(Conversation))]
    public long ConversationId { get; set; }
    public Conversation Conversation { get; set; }
    public string Name { get; set; }
}