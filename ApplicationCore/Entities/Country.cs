﻿using System.ComponentModel.DataAnnotations;

namespace ApplicationCore.Entities
{
    public class Country
    {
        [Key]
        [Required]
        [MaxLength(2)]
        public string Code { get; set; }
        [Required]
        [MaxLength(60)]
        public string Name { get; set; }
    }
}
