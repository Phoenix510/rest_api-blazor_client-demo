using System.Net.WebSockets;

namespace ApplicationCore.Services;

public class WebSocketClient
{
    public WebSocketClient(string username, WebSocket connection)
    {
        Username = username;
        Connection = connection;
        LastActive = DateTimeOffset.UtcNow;
    }
    
    public string Username { get; set; }
    public WebSocket Connection { get; set; }
    public DateTimeOffset LastActive { get; set; }
}