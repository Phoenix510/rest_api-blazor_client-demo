﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApplicationCore.Entities
{
    public class UserSession
    {
        [Key]
        public string Token { get; set; }
        [Required]
        public DateTimeOffset CreationDate { get; set; }
        [Required]
        public DateTimeOffset ExpirationDate { get; set; }
        [Required]
        public string TokenType { get; set; }
        [Required]
        public uint IP { get; set; }

        [ForeignKey(nameof(User))]
        public long UserId { get; set; }
        public User User { get; set; }

        public SessionGeolocation? Geolocation { get; set; }
    }

    public class SessionGeolocation
    {
        [Key]
        [ForeignKey(nameof(UserSession))]
        public string Token { get; set; }
        public UserSession UserSession { get; set; }

        public string? Region { get; set; }

        [ForeignKey(nameof(Country))]
        public string CountryCode { get; set; }
        public Country? Country { get; set; }

        [ForeignKey(nameof(Continent))]
        public string ContinentCode { get; set; }
        public Continent? Continent { get; set; }
    }
}
