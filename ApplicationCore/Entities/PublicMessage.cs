namespace ApplicationCore.Entities;

public class PublicMessage : Message
{
    public ICollection<PublicMessage> Responses { get; set; }
}