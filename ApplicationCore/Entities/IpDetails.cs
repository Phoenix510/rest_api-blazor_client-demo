﻿namespace ApplicationCore.Entities
{
    public class IpDetails
    {
        public string Ip { get; set; }
        public string Continent_Name { get; set; }
        public string Continent_Code { get; set; }
        public string Country_Name { get; set; }
        public string Country_Code { get; set; }
        public string Region { get; set; }
        public string Region_Code { get; set; }
        public string City { get; set; }
    }
}
