using ApplicationCore.Entities;
using AutoMapper;
using BlazorShared.Contracts.V1;
using BlazorShared.Contracts.V1.DTO;

namespace ApplicationCore.Mapping;

public class MappingProfiles : Profile
{
    public MappingProfiles()
    {
        CreateMap<Message, MessageDTO>()
            .ForMember(dest => dest.CreatedBy,
                opt => opt.MapFrom(src => src.Creator.Username));

        CreateMap<DirectMessage, DirectMessageDTO>()
            .ForMember(dest => dest.CreatedBy,
                opt => opt.MapFrom(src => src.Creator.Username));


        CreateMap<PublicMessage, PublicMessageDTO>()
            .ForMember(dest => dest.CreatedBy,
                opt => opt.MapFrom(src => src.Creator.Username))
            .ForMember(dest => dest.Responses,
                opt => opt.MapFrom(src => src.Responses));

        CreateMap<User, UserDTO>();
        CreateMap<User, SearchByUsernameRecord>();

        CreateMap<Conversation, ConversationDTO>()
            .ForMember(dest => dest.Name,
                opt => opt.MapFrom(src =>
                    src.ConversationUserDetailsCollection.First().Name))
            .ForMember(dest => dest.Participants,
                opt => opt.MapFrom(src =>
                    src.Participants.Select(p => p.Username).ToList()));

        CreateMap<UserSession, UserSessionDTO>()
            .ForMember(dest => dest.Country,
                opt => opt.MapFrom(src =>
                    src.Geolocation != null ? src.Geolocation.Country.Name : "Unknown"))
            .ForMember(dest => dest.Region,
                opt => opt.MapFrom(src =>
                    src.Geolocation != null ? src.Geolocation.Region : "Unknown"))
            .ForMember(dest => dest.Continent,
                opt => opt.MapFrom(src =>
                    src.Geolocation != null ? src.Geolocation.Continent.Name : "Unknown"));
    }
}