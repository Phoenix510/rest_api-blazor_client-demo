﻿using System.ComponentModel.DataAnnotations;

namespace ApplicationCore.Options
{
    public class JwtOptions
    {
        public const string Name = "JwtSettings";

        [Required]

        public required string Issuer { get; init; }
        [Required]

        public required string Audience { get; init; }
        [Required]

        public required string Key { get; init; }
    }
}
