﻿using System.ComponentModel.DataAnnotations;

namespace ApplicationCore.Options
{
    public class AppOptions
    {
        public const string Name = "AppOptions";
        [Required]

        public required string[] Allowed_URIs { get; init; }
        [Required]

        public required string Geo_Api_Key { get; init; }
    }
}
