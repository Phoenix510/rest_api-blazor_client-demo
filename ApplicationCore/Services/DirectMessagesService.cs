using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using AutoMapper;
using BlazorShared.Contracts.V1;
using BlazorShared.WebSocketComm;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace ApplicationCore.Services;

public class DirectMessagesService : IDirectMessagesService
{
    private readonly IRepository<DirectMessage> messagesRepository;
    private readonly IRepository<Conversation> conversationRepository;
    private readonly IMapper mapper;
    private readonly IWebSocketClientsService webSocketClientsService;

    public DirectMessagesService(IRepository<DirectMessage> messagesRepository,
        IRepository<Conversation> conversationRepository,
        IMapper mapper,
        IWebSocketClientsService webSocketClientsService)
    {
        this.messagesRepository = messagesRepository;
        this.conversationRepository = conversationRepository;
        this.mapper = mapper;
        this.webSocketClientsService = webSocketClientsService;
    }

    public async Task<(IEnumerable<string> errors, DirectMessageDTO message)> Insert
        (CreateDirectMessageModel createDirectMessageModel, User creator)
    {
        var newMessage = new DirectMessage()
            { Content = createDirectMessageModel.Content, CreationDate = DateTimeOffset.UtcNow, Creator = creator };

        var conversation = conversationRepository.Find(createDirectMessageModel.ConversationId);
        if (conversation is null)
            return (new[] { "Conversation does not exist" }, null);

        newMessage.ConversationId = conversation.Id;
        conversation.LastModification = DateTimeOffset.UtcNow;

        conversationRepository.Update(conversation);
        messagesRepository.Insert(newMessage);
        await messagesRepository.SaveChangesAsync();
        await conversationRepository.SaveChangesAsync();

        InformInterestedUsers(newMessage, conversationRepository.Query
            .Where(x => x.Id == newMessage.ConversationId)
            .Include(x => x.Participants).Select(x => x.Participants.Select(u => u.Username)).First());
        
        return (Enumerable.Empty<string>(), mapper.Map<DirectMessage, DirectMessageDTO>(newMessage));
    }
    
    private void InformInterestedUsers(DirectMessage message, IEnumerable<string> usernames)
    {
        var messEvent = new MessageEvent<DirectMessageDTO>(mapper.Map<DirectMessageDTO>(message),  MessageType.Sent, nameof(DirectMessageDTO));
        foreach (var username in usernames)
        {
            _ = webSocketClientsService.Send(JsonConvert.SerializeObject(messEvent), username);
        }
    }
}