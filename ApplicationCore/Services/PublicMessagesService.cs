using System.Text.Json.Serialization;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using AutoMapper;
using BlazorShared.Contracts.V1;
using BlazorShared.WebSocketComm;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace ApplicationCore.Services;

public class PublicMessagesService : IPublicMessagesService
{
    private readonly IWebSocketClientsService webSocketClientsService;
    private readonly IMapper mapper;
    private readonly IRepository<PublicMessage> messagesRepository;

    public PublicMessagesService(IRepository<PublicMessage> messagesRepository, 
        IWebSocketClientsService webSocketClientsService, IMapper mapper)
    {
        this.webSocketClientsService = webSocketClientsService;
        this.mapper = mapper;
        this.messagesRepository = messagesRepository;
    }

    public async Task<IEnumerable<PublicMessage>> GetAll(int pageSize, int pageNumber)
    {
        var skip = (pageNumber - 1) * pageSize;
        return await
            messagesRepository.Query
                .Include(x => x.Creator)
                .Include(x => x.Responses)
                .OrderByDescending(x => x.CreationDate)
                .Skip(skip)
                .Take(pageSize)
                .ToListAsync();
    }

    public async Task<(IEnumerable<string> errors, PublicMessage message)> Insert(CreatePublicMessageModel createPublicMessageModel,
        User creator)
    {
        var newMessage = new PublicMessage()
            { Content = createPublicMessageModel.Content, CreationDate = DateTimeOffset.UtcNow, Creator = creator };

        if (!createPublicMessageModel.ResponseToId.HasValue)
        {
            messagesRepository.Insert(newMessage);
            await messagesRepository.SaveChangesAsync();
            InformConnectedUsers(newMessage);
            return (Enumerable.Empty<string>(), newMessage);
        }

        var parentMessage = messagesRepository.Query
            .Include(x => x.Responses)
            .FirstOrDefault(x => x.Id == createPublicMessageModel.ResponseToId.Value);

        if (parentMessage is null)
            return (new string[] { "Message to which the reply relates does not exist" }, null);

        parentMessage.Responses.Add(newMessage);
        messagesRepository.Insert(newMessage);
        messagesRepository.Update(parentMessage);

        await messagesRepository.SaveChangesAsync();
        InformConnectedUsers(newMessage);
        
        return (Enumerable.Empty<string>(), newMessage);
    }

    // it should be moved to some new class
    private void InformConnectedUsers(PublicMessage publicMessage)
    {
        var messEvent = new MessageEvent<PublicMessageDTO>(mapper.Map<PublicMessageDTO>(publicMessage),  MessageType.Sent, nameof(PublicMessageDTO));
        _ = webSocketClientsService.Send(JsonConvert.SerializeObject(messEvent), "ALL");
    }
}