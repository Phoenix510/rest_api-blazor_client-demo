using ApplicationCore.Entities;
using ApplicationCore.Extensions;
using ApplicationCore.Interfaces;
using BlazorShared;
using BlazorShared.Contracts.V1;
using BC = BCrypt.Net;
using User = ApplicationCore.Entities.User;

namespace ApplicationCore.Services;

public class SingUpService: ISingUpService
{
    private readonly IRepository<User> userRepository;

    public SingUpService(IRepository<User> userRepository)
    {
        this.userRepository = userRepository;
    }

    public(string detail, IEnumerable<string> errors) SingUp(CreateUserModel user)
    {
        var validationResult = UserDataValidator.ValidateData(user.Username, user.Password, user.Email);
        if (!validationResult.Success)
            return ("Data is invalid", validationResult.Errors);

        if (userRepository.GetByUsername(user.Username) is not null)
            return ("Username is already taken!", Enumerable.Empty<string>());
        
        if (userRepository.GetByEmail(user.Email) is not null)
            return ("This email is already in use!", Enumerable.Empty<string>());
        
        string passwordHash =  BC.BCrypt.HashPassword(user.Password);
        var newUser = new User() { Username = user.Username, Password = passwordHash, Email = user.Email };
        
        userRepository.Insert(newUser);

        return ("User has been created!", Enumerable.Empty<string>());
    }
}