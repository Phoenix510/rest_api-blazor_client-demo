﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Http;
using System.Net;

namespace ApplicationCore.Services
{
    public class IPDetailsResolver : IIPDetailsResolver
    {
        private readonly IGeolocationService geolocationService;

        public IPDetailsResolver(IGeolocationService geolocationService)
        {
            this.geolocationService = geolocationService;
        }

        public async Task<IpDetails?> GetIPDetails(IPAddress ip)
        {
            if (ip != null)
                return await geolocationService.GetIpDetails(ip.ToString());
            else
                return null;
        }
    }
}
