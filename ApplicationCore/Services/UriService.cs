using ApplicationCore.Interfaces;
using BlazorShared.Contracts.V1;
using BlazorShared.Contracts.V1.DTO;

namespace ApplicationCore.Services;

public class UriService: IUriService
{
    private readonly string baseUri;

    public UriService(string baseUri)
    {
        this.baseUri = baseUri;
    }
    
    public Uri GetAllPublicMessagesUri(PageRequest pageRequest)
    {
        var uri = new Uri(baseUri);
        var outUri = uri + ApiRoutes.PublicMessages.GetAll +  $"?pageNumber={pageRequest.PageNumber}&pageSize={pageRequest.PageSize}";
        
        return new Uri(outUri);
    }
    
    public Uri GetMessagesFromConversationUri(long conversationId, PageRequest pageRequest)
    {
        var uri = new Uri(baseUri);
        var endpUri = ApiRoutes.Conversation.GetMessagesFromConversation.Replace("{conversationId:long}", conversationId.ToString());
        var outUri = uri + endpUri +  $"?pageNumber={pageRequest.PageNumber}&pageSize={pageRequest.PageSize}";
        
        return new Uri(outUri);
    }
}