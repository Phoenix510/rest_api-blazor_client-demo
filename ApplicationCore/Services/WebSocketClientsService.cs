using System.Net.Sockets;
using System.Net.WebSockets;
using System.Text;

namespace ApplicationCore.Services;

public class WebSocketClientsService : IWebSocketClientsService
{
    public List<WebSocketClient> clients = new();

    public WebSocketClientsService()
    {

    }

    public async Task<IEnumerable<string>> AddClientConnection(WebSocket client, string ownerUsername)
    {
        if (clients.Any(x => x.Username == ownerUsername))
            return new[] { $"Connection was already established for {ownerUsername}" };
        var newClient = new WebSocketClient(ownerUsername, client);
        clients.Add(newClient);

        // this will keep connection alive until the close message will be received - ReceiveMessages function will free the thread
        await ReceiveMessages(newClient);
        return Enumerable.Empty<string>();
    }

    public void RemoveClientConnection(string ownerUsername)
    {
        var client = clients.FirstOrDefault(x => x.Username == ownerUsername);
        if (client is not null)
            clients.Remove(client);
    }

    async Task ReceiveMessages(WebSocketClient client)
    {
        try
        {
            while (client.Connection.State == WebSocketState.Open)
            {
                var buffer = new byte[1024];
                while (true)
                {
                    string message = string.Empty;
                    var result =
                        await client.Connection.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);

                    if (result.MessageType == WebSocketMessageType.Close)
                    {
                        clients.Remove(client);
                        await client.Connection.CloseAsync(result.CloseStatus.Value, result.CloseStatusDescription,
                            CancellationToken.None);
                        break;
                    }

                    message += Encoding.UTF8.GetString(buffer, 0, result.Count);

                    //if (result.EndOfMessage)
                    // here message will be processed in the future
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    public async Task Send(string message, string recipientUsername = "ALL")
    {
        var bytes = Encoding.UTF8.GetBytes(message);

        var recipients = clients;
        if (recipientUsername.ToUpper() != "ALL")
        {
            var recipient = clients.FirstOrDefault(x => string.Equals
                    (x.Username, recipientUsername, StringComparison.InvariantCultureIgnoreCase));
            if (recipient is null)
                return;
            recipients = new List<WebSocketClient>() { recipient };
        }

        foreach (var recipient in recipients)
        {
            var arraySegment = new ArraySegment<byte>(bytes, 0, bytes.Length);
            await recipient.Connection.SendAsync(arraySegment, WebSocketMessageType.Text, true, CancellationToken.None);
        }
    }

    public void RefreshLastActivityTime(string username)
    {
        var client = clients.FirstOrDefault(x => x.Username == username);
        if (client is null)
            return;
        client.LastActive = DateTimeOffset.UtcNow;
    }
}