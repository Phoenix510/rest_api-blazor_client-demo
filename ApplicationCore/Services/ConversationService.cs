using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using BlazorShared.Contracts.V1.DTO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ApplicationCore.Services;

public class ConversationService : IConversationService
{
    private readonly IRepository<DirectMessage> messagesRepository;
    private readonly IRepository<Conversation> conversationRepository;
    private readonly IRepository<User> userRepository;

    public ConversationService(IRepository<DirectMessage> messagesRepository,
        IRepository<Conversation> conversationRepository, IRepository<User> userRepository)
    {
        this.messagesRepository = messagesRepository;
        this.conversationRepository = conversationRepository;
        this.userRepository = userRepository;
    }

    public (IEnumerable<string> errors, Conversation conversation) Insert
        (CreateConversationModel createConversationModel, User creator)
    {
        if (creator.Id == createConversationModel.ParticipantId)
            return (new[] { "You cannot send message to yourself!" }, null)!;

        var participant = userRepository.Find(createConversationModel.ParticipantId);
        if (participant is null)
            return (new[] { "Participant does not exist!" }, null)!;

        creator = userRepository.Find(creator.Id);
        if (creator is null)
            return (new[] { "Creator does not exist!" }, null)!;

        var existingConversation = conversationRepository.Query.FirstOrDefault(x =>
            x.Participants.Any(x => x.Id == creator.Id) &&
            x.Participants.Any(x => x.Id == participant.Id) &&
            x.Participants.Count() == 2);

        if (existingConversation is not null)
            return (Enumerable.Empty<string>(), existingConversation);

        var newConversation = new Conversation()
        {
            Participants = new List<User>() { creator, participant },
            LastModification = DateTimeOffset.UtcNow,
            ConversationUserDetailsCollection = new List<ConversationUserDetails>()
            {
                new ConversationUserDetails() { Owner = creator, Name = createConversationModel.Name },
                new ConversationUserDetails() { Owner = participant, Name = creator.Username }
            }
        };

        conversationRepository.Insert(newConversation);

        return (Enumerable.Empty<string>(), newConversation);
    }

    public async Task<IEnumerable<DirectMessage>> GetMessagesFromConversation(long conversationId, long userId,
        int pageSize, int pageNumber)
    {
        var skip = (pageNumber - 1) * pageSize;

        var userIsAPartOfConv = conversationRepository.Query
            .Where(x => x.Id == conversationId)
            .Include(x => x.Participants)
            .Any(x => x.Participants.Any(u => u.Id == userId));

        if (!userIsAPartOfConv)
            return new List<DirectMessage>();

        return await messagesRepository.Query
            .Where(x => x.ConversationId == conversationId)
            .OrderByDescending(x => x.CreationDate)
            .Skip(skip)
            .Take(pageSize)
            .Include(x => x.Creator)
            .ToListAsync();
    }

    public async Task<Conversation?> GetConversationDetails(long userId, long conversationId)
    {
        return await conversationRepository.Query
            .Where(x => x.Id == conversationId)
            .Include(x => x.Participants)
            .Where(x => x.Participants.Any(u => u.Id == userId))
            .Include(x => x.ConversationUserDetailsCollection.Where(c => c.OwnerId == userId))
            .Include(x => x.Messages.OrderByDescending(mess => mess.CreationDate).Take(1))
            .ThenInclude(x => x.Creator)
            .FirstOrDefaultAsync();
    }

    public async Task<IEnumerable<Conversation>> GetLastConversationsWithLastMessage(long userId,
        int numOfConversations = 10)
    {
        return await conversationRepository.Query
            .Include(x => x.Participants)
            .Where(x => x.Participants.Any(u => u.Id == userId))
            .Include(x => x.ConversationUserDetailsCollection.Where(c => c.OwnerId == userId))
            .Include(x => x.Messages.OrderByDescending(mess => mess.CreationDate).Take(1))
            .OrderByDescending(x => x.LastModification)
            .Take(10)
            .ToListAsync();
    }
}