﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using AutoMapper;
using BlazorShared.Contracts.V1.DTO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace ApplicationCore.Services
{
    public class UserSessionsService : IUserSessionsService
    {
        private readonly IRepository<UserSession> sessionRepository;
        private readonly IRepository<User> userRepository;
        private readonly ILogger<UserSessionsService> logger;
        private readonly IMapper mapper;

        public UserSessionsService(IRepository<UserSession> sessionRepository, 
            IRepository<User> userRepository,
            ILogger<UserSessionsService> logger, 
            IMapper mapper)
        {
            this.sessionRepository = sessionRepository;
            this.userRepository = userRepository;
            this.logger = logger;
            this.mapper = mapper;
        }

        public async Task ClearExpiredSessions()
        {
            var expired = await sessionRepository.Query
               .Where(x => x.ExpirationDate < DateTimeOffset.UtcNow)
               .Select(x => new UserSession() { Token = x.Token })
               .ToListAsync();

            foreach (var session in expired)
            {
                sessionRepository.Delete(session);
            }

            await sessionRepository.SaveChangesAsync();
            logger.LogInformation("Deleted {0} expired sessions", expired.Count());
        }

        public async Task<IEnumerable<UserSessionDTO>> GetSessionsFor(string username, int pageSize, int pageNumber)
        {
            var userId = await userRepository.Query.Where(u => u.Username == username)
                .Select(x => x.Id)
                .SingleOrDefaultAsync();

            var skip = (pageNumber - 1) * pageSize;

            var userSessions = await sessionRepository.Query
                .Where(x => x.UserId == userId)
                .OrderByDescending(x => x.CreationDate)
                .Skip(skip)
                .Take(pageSize)
                .Include( x=> x.Geolocation)
                    .ThenInclude( x=> x.Continent)
                 .Include(x => x.Geolocation)
                    .ThenInclude(x => x.Country)
                .Select(x => mapper.Map<UserSessionDTO>(x))
                .ToListAsync();

            return userSessions;
        }

        public async Task<(bool, string)> DeleteSession(string refreshToken, long userId)
        {
            var session = await sessionRepository.Query
                .SingleOrDefaultAsync(x => x.Token == refreshToken && x.UserId == userId);

            if (session is null)
                return (false, "Invalid refresh token");

            sessionRepository.Delete(session);
            await sessionRepository.SaveChangesAsync();

            return (true, string.Empty);
        }
    }
}
