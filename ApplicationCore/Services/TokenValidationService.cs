using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using ApplicationCore.Interfaces;
using ApplicationCore.JWT;
using Microsoft.IdentityModel.Tokens;

namespace ApplicationCore.Services;

public class TokenValidationService: ITokenValidationService
{
    private readonly TokenValidationParameters tokenValidationParameters;

    public TokenValidationService(JWTSettings jWTSettings)
    {
        this.tokenValidationParameters = jWTSettings.GetTokenValidationParameters();
    }

    public ClaimsPrincipal? GetClaimsPrincipalFromToken(string token)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        try
        {
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken validatedToken);
            if (!IsJwtValidSecurityAlgorithm(validatedToken))
                return null;
            return principal;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return null;
        }
    }
    
    private bool IsJwtValidSecurityAlgorithm(SecurityToken securityToken)
    {
        return (securityToken is JwtSecurityToken jwtSecurityToken) &&
               jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256Signature,
                   StringComparison.InvariantCultureIgnoreCase);
    }
}