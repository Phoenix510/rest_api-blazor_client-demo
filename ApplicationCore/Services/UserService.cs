using ApplicationCore.Entities;
using ApplicationCore.Interfaces;

namespace ApplicationCore.Services;

public class UserService: IUserService
{
    private readonly IRepository<User> userRepository;

    public UserService(IRepository<User> userRepository)
    {
        this.userRepository = userRepository;
    }

    public IEnumerable<User> GetUsersStartingWith(string usernamePart, int count = 10)
    {
        return userRepository.Query
            .Where(x => x.Username.StartsWith(usernamePart))
            .OrderBy(x => x.Username)
            .Take(count)
            .ToList();
    }
}