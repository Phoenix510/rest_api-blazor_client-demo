using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.JWT;
using BlazorShared.Contracts.V1;
using BlazorShared.Converters;
using Microsoft.EntityFrameworkCore;
using System.Net;
using BC = BCrypt.Net;
using User = ApplicationCore.Entities.User;

namespace ApplicationCore.Services;

public class AuthenticationService : IAuthenticationService
{
    private readonly IRepository<User> userRepository;
    private readonly IJWTManager jwtManager;
    private readonly IRepository<UserSession> userSessionRepository;
    private readonly IRepository<Continent> continentRepository;
    private readonly IRepository<Country> countryRepository;
    private readonly IIPDetailsResolver ipDetailsResolver;
    private readonly IIPConverter ipConverter;

    public AuthenticationService(IRepository<User> userRepository,
        IJWTManager jwtManager,
        IRepository<UserSession> userSessionRepository,
        IRepository<Continent> continentRepository,
        IRepository<Country> countryRepository,
        IIPDetailsResolver ipDetailsResolver,
        IIPConverter ipConverter)
    {
        this.userRepository = userRepository;
        this.jwtManager = jwtManager;
        this.userSessionRepository = userSessionRepository;
        this.continentRepository = continentRepository;
        this.countryRepository = countryRepository;
        this.ipDetailsResolver = ipDetailsResolver;
        this.ipConverter = ipConverter;
    }

    public async Task<(bool valid, TokenModel token)> AuthUser(string username, string password, IPAddress ip)
    {
        var userData = userRepository.Query
            .Where(x => x.Username == username)
            .Select(x => new { x.Password, x.Id })
            .FirstOrDefault();

        if (userData is null || !BC.BCrypt.Verify(password, userData.Password))
            return (false, null!);

        string accessToken = jwtManager.GenerateAccessToken(username);
        string refreshToken = jwtManager.GenerateRefreshToken(username);

        var session = await CreateSession(userData.Id, refreshToken, ip);
        userSessionRepository.Insert(session);
        await userSessionRepository.SaveChangesAsync();

        return (true, new TokenModel() { AccessToken = accessToken, RefreshToken = refreshToken });
    }

    public async Task<(bool valid, TokenModel token)> RefreshSession(string refreshToken, IPAddress ip)
    {
        var session = userSessionRepository.Query
            .Include(x => x.Geolocation)
                    .ThenInclude(x => x.Continent)
            .Include(x => x.Geolocation)
                    .ThenInclude(x => x.Country)
            .FirstOrDefault(x => x.Token == refreshToken);

        if (session is null || session.TokenType != JWTManager.REFRESH_TOKEN)
            return (false, null);
        var username = userRepository.Query.Where(x => x.Id == session.UserId).Select(x => x.Username).FirstOrDefault();
        if (username is null)
            return (false, null);

        string accessToken = jwtManager.GenerateAccessToken(username);
        string newRefreshToken = jwtManager.GenerateRefreshToken(username);

        userSessionRepository.Delete(session);


        UserSession newSession = null;
        if (session.IP == ipConverter.GetIntFromIP(ip))
        {
            newSession = await CreateSession(session.UserId, newRefreshToken, null);
            if (session.Geolocation != null)
            {
                newSession.Geolocation = session.Geolocation;
                newSession.Geolocation.Token = newRefreshToken;
            }
            newSession.IP = session.IP;
        }
        else
            newSession = await CreateSession(session.UserId, newRefreshToken, ip);


        userSessionRepository.Insert(newSession);
        await userSessionRepository.SaveChangesAsync();

        return (true, new TokenModel() { AccessToken = accessToken, RefreshToken = newRefreshToken });
    }

    private async Task<UserSession> CreateSession(long userId, string token, IPAddress ip)
    {
        var session = new UserSession()
        {
            UserId = userId,
            CreationDate = DateTimeOffset.UtcNow,
            ExpirationDate = DateTimeOffset.UtcNow.AddMinutes(JWTManager.REFRESH_TOKEN_LIFETIME_IN_MINUTES),
            Token = token,
            TokenType = JWTManager.REFRESH_TOKEN
        };

        //if ip is null that means a) somehow ip has been unable to be get from request
        // b) details of ip are already known (e.g. when token is refreshed)

        if (ip != null)
        {
            session.IP = ipConverter.GetIntFromIP(ip);
            var details = await ipDetailsResolver.GetIPDetails(ip);

            if (details is not null)
            {
                var continent = continentRepository.Find(details.Continent_Code) ??
                    new() { Code = details.Continent_Code, Name = details.Continent_Name };

                var country = countryRepository.Find(details.Country_Code) ??
                    new() { Code = details.Country_Code, Name = details.Country_Name };

                session.Geolocation = new SessionGeolocation()
                {
                    Continent = continent,
                    Country = country,
                    Region = details.Region
                };
            }
        }

        return session;
    }
}