﻿using System.Net;

namespace BlazorShared.Converters
{
    public interface IIPConverter
    {
        uint GetIntFromIP(IPAddress ipAddress);
        IPAddress GetIPFromInt(uint ipAddress);
    }
}