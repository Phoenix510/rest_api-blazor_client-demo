﻿using System.Net;

namespace BlazorShared.Converters
{
    public class IPConverter : IIPConverter
    {
        public IPAddress GetIPFromInt(uint ipAddress)
        {
            return new IPAddress(BitConverter.GetBytes(ipAddress).Reverse().ToArray());
        }

        public uint GetIntFromIP(IPAddress ipAddress)
        {
            return BitConverter.ToUInt32(ipAddress.MapToIPv4().GetAddressBytes().Reverse().ToArray());
        }
    }
}
