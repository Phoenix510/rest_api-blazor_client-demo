using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace BlazorShared;

public class UserDataValidator
{
    public const int PasswordMinLength = 8;
    public const int PasswordMaxLength = 64;

    public const int UsernameMinLength = 6;
    public const int UsernameMaxLength = 20;

    public static ValidationResult IsValidEmail(string email)
    {
        if (string.IsNullOrWhiteSpace(email) || !new EmailAddressAttribute().IsValid(email))
            return new ValidationResult(false, new[] { "Email is invalid or empty" });
        return new ValidationResult(true);
    }

    public static ValidationResult IsValidUsername(string username)
    {
        var errors = new List<string>();
        if (username.Length < UsernameMinLength)
            errors.Add($"Username has to have at least {UsernameMinLength} characters");
        else if (username.Length > UsernameMaxLength)
            errors.Add($"Username has to have maximum {UsernameMaxLength} characters");

        var regex = new Regex("^[A-Za-z][A-Za-z0-9_/.]*$");
        if (!regex.IsMatch(username))
            errors.Add($"Username has to start with an alphabet character.");

        if (!errors.Any())
            return new ValidationResult(true);
        return new ValidationResult(false, errors);
    }

    public static ValidationResult IsValidPassword(string password)
    {
        var errors = new List<string>();
        if (string.IsNullOrEmpty(password) || password.Length is < PasswordMinLength or > PasswordMaxLength)
            errors.Add($"The password has to have a length between {PasswordMinLength}-{PasswordMaxLength}");

        // credits https://www.c-sharpcorner.com/uploadfile/jitendra1987/password-validator-in-C-Sharp/
        var regex = new Regex(@"^.*(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!*@#$%^&+=]).*$");
        if (!regex.IsMatch(password))
            errors.Add(
                $"Password has to contain at least one lower case letter, one upper, one special character, one number");

        if (!errors.Any())
            return new ValidationResult(true);
        return new ValidationResult(false, errors);
    }

    public static ValidationResult ValidateData(string username, string password, string email)
    {
        var errors = new List<string>();
        errors.AddRange(IsValidUsername(username).Errors ?? Enumerable.Empty<string>());
        errors.AddRange(IsValidPassword(password).Errors ?? Enumerable.Empty<string>());
        errors.AddRange(IsValidEmail(email).Errors ?? Enumerable.Empty<string>());

        if (!errors.Any())
            return new ValidationResult(true);
        return new ValidationResult(false, errors);
    }
}