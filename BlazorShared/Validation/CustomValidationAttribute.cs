using System.ComponentModel.DataAnnotations;

namespace BlazorShared;

public enum ValidationType
{
    Email,
    Password,
    Username
}

[AttributeUsage(AttributeTargets.Property | 
                AttributeTargets.Field, AllowMultiple = false)]
sealed public class CustomValidationAttribute : ValidationAttribute
{
    private readonly ValidationType validationType;
    private readonly Func<string,ValidationResult> validFunc;

    public CustomValidationAttribute(ValidationType validationType)
    {
        this.validationType = validationType;
        switch (validationType)
        {
            case ValidationType.Email:
                validFunc = UserDataValidator.IsValidEmail;
                break;
            case ValidationType.Password:
                validFunc = UserDataValidator.IsValidPassword;
                break;
            case ValidationType.Username:
                validFunc = UserDataValidator.IsValidUsername;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
    
    public override bool IsValid(object value)
    {
        return validFunc(value.ToString()).Success;
    }
}