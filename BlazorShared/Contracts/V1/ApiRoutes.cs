namespace BlazorShared.Contracts.V1;

public static class ApiRoutes
{
    public const string Root = "api";
    public const string Version = "v1";
    public const string Base = $"{Root}/{Version}";

    public static class Authentication
    {
        public const string Login = $"{Base}/authentication/Login";
        public const string RefreshSession = $"{Base}/authentication/RefreshSession";
    }
    
    public static class Conversation
    {
        public const string GetLastConversations = $"{Base}/Conversation/GetLastConversations";
        public const string GetConversationDetails = Base + "/Conversation/GetConversationDetails/{conversationId:long}";
        public const string CreateNewConversation = Base + "/Conversation/CreateNewConversation";
        public const string GetMessagesFromConversation = Base +"/Conversation/GetMessagesFromConversation/{conversationId:long}";
    }
    
    public static class DirectMessages
    {
        public const string Insert = $"{Base}/DirectMessages/Insert";
    }
    
    public static class PublicMessages
    {
        public const string GetAll = $"{Base}/PublicMessages/GetAll";
        public const string Insert = $"{Base}/PublicMessages/Insert";
    }

    public static class Users
    {
        public const string GetUserDetails = $"{Base}/users/GetUserDetails";
        public const string GetUsersWhichUsernameStartsWith = $"{Base}/users/GetUsersWhichUsernameStartsWith";
        public const string SingUp = $"{Base}/users/SingUp";
    }

    public static class MessagesWSController
    {
        public const string EstablishConnection = Base + "/MessagesWS/EstablishConnection";
    }

    public static class UserSessions
    {
        public const string GetUserSessions = Base + "/UserSessions/GetUserSessions";
        public const string DeleteSession = $"{Base}/UserSessions/DeleteSession";
    }
}