namespace BlazorShared.Contracts.V1;

public class ApiResponse<T>
{
    public string ResponseStatus { get; set; }
    public T Data { get; set; }

    public ApiResponse(){}
    
    protected ApiResponse(string responseStatus, T data  = default(T))
    {
        ResponseStatus = responseStatus;
        Data = data;
    }
    
    public static ApiResponse<T> SuccessResponse(T data = default(T)) => new ApiResponse<T>(Consts.ResponseStatus.Success, data);
    public static ApiResponse<T> FailResponse(string detail, T data = default(T)) => new FailResponse<T>(Consts.ResponseStatus.Fail, detail, data);
    public static ApiResponse<T> ErrorResponse(string type, string title, T data = default(T)) => new ErrorResponse<T>(Consts.ResponseStatus.Error, type, title, data);
    public static ApiResponse<T> ErrorResponse(string type, string title, string detail, T data = default(T)) => new ErrorResponse<T>(Consts.ResponseStatus.Error, type, title, detail, data);
}

public class FailResponse<T>: ApiResponse<T>
{
    public string Detail { get; set; }
    
    public FailResponse(string responseStatus, string detail, T data = default(T)) : base(responseStatus, data)
    {
        Detail = detail;
    }
}

public class ErrorResponse<T>: ApiResponse<T>
{
    public string Title { get; set; }
    public string Type { get; set; }
    public string Detail { get; set; }

    public ErrorResponse(string responseStatus, string type, string title, T data = default(T)) : base(responseStatus, data)
    {
        Type = type;
        Title = title;
        Detail = string.Empty;
    }
    
    public ErrorResponse(string responseStatus, string type, string title, string detail,  T data) : base(responseStatus, data)
    {
        Type = type;
        Title = title;
        Detail = detail;
    }
}