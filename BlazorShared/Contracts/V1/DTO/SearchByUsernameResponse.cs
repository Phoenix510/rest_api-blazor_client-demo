namespace BlazorShared.Contracts.V1.DTO;

public class SearchByUsernameRecord
{
    public long Id { get; set; }
    public string Username { get; set; }
}