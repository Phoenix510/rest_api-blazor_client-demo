﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlazorShared.Contracts.V1.DTO
{
    public class UserSessionDTO
    {
        public string Token { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string Continent { get; set; }
        public uint Ip { get; set; }
        public string TokenType { get; set; }
        public DateTimeOffset CreationDate { get; set; }
        public DateTimeOffset ExpirationDate { get; set; }
    }
}
