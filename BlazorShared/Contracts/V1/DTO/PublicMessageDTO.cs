namespace BlazorShared.Contracts.V1;

public class PublicMessageDTO : MessageDTO
{
    public ICollection<PublicMessageDTO> Responses { get; set; }
}