using System.ComponentModel.DataAnnotations;

namespace BlazorShared.Contracts.V1;

public class CreateMessageModel
{
    [Required] [MaxLength(320)] public string Content { get; set; }
}

public class CreateDirectMessageModel : CreateMessageModel
{
    [Required] public long ConversationId { get; set; }
}

public class CreatePublicMessageModel : CreateMessageModel
{
    public long? ResponseToId { get; set; }
}