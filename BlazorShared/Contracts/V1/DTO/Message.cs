namespace BlazorShared.Contracts.V1;

public class MessageDTO
{
    public long Id { get; set; }
    public string Content { get; set; }
    public string CreatedBy { get; set; }
    public DateTimeOffset CreationDate { get; set; }
}