namespace BlazorShared;

public class ValidationResult
{
    public readonly bool Success;
    public readonly IEnumerable<string>? Errors;
    public ValidationResult(bool success, IEnumerable<string> errors = null)
    {
        Success = success;
        Errors = errors;
    }
}