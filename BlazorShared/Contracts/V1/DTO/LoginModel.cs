using System.ComponentModel.DataAnnotations;

namespace BlazorShared.Contracts.V1;

public class LoginModel
{
    [Required(ErrorMessage = "Username is required.")]
    [CustomValidation(ValidationType.Username)]
    public string Username { get; set; }
    
    [Required(ErrorMessage = "Password is required.")]
    [CustomValidation(ValidationType.Password)]
    public string Password { get; set; }
}