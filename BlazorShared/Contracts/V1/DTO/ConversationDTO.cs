namespace BlazorShared.Contracts.V1;

public class ConversationDTO
{
    public long Id { get; set; }
    public ICollection<string> Participants { get; set; }
    public ICollection<DirectMessageDTO> Messages { get; set; }
    public string Name { get; set; }
}