namespace BlazorShared.Contracts.V1.DTO;

public class CreateConversationModel
{
    public string Name { get; set; }
    
    public long ParticipantId { get; set; }
}