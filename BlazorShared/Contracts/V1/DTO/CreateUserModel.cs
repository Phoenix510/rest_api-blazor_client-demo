using System.ComponentModel.DataAnnotations;

namespace BlazorShared.Contracts.V1;

public class CreateUserModel: LoginModel
{
    [Required(ErrorMessage = "Email is required.")]
    [EmailAddress(ErrorMessage = "Email address is not valid.")]
    [CustomValidation(ValidationType.Email)]
    public string Email { get; set; }
    
    [Required]
    [DataType(DataType.Password)]
    [Compare(nameof(Password), ErrorMessage = "The passwords do not match.")]
    public string? ConfirmPassword { get; set; }
}