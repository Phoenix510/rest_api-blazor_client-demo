using System.ComponentModel.DataAnnotations;

namespace BlazorShared.Contracts.V1.DTO;

public class PageRequest
{
    public PageRequest()
    {
        PageSize = 40; PageNumber = 1;
    }
    public PageRequest(int pageSize, int pageNumber)
    {
        PageSize = pageSize;
        PageNumber = pageNumber;
    }

    [Range(1, 100)]
    public int PageSize { get; set; }
    [Range(1, int.MaxValue)]
    public int PageNumber { get; set; }
}