using System.ComponentModel.DataAnnotations;

namespace BlazorShared.Contracts.V1.DTO;

public class SerachUserByUsernameModel
{
    [Required]
    [MinLength(3)]
    [MaxLength(UserDataValidator.UsernameMaxLength)]
    public string Username { get; set; } = string.Empty;
}