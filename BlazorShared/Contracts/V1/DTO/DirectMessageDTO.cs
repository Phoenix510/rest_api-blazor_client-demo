namespace BlazorShared.Contracts.V1;

public class DirectMessageDTO : MessageDTO
{
    public long ConversationId { get; set; }
    public bool Read { get; set; }
    public bool Received { get; set; }
}