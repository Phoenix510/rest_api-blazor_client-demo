namespace BlazorShared.Contracts.V1;

public class UserDTO
{
    public long Id { get; set; }
    public string Username { get; set; }
    public string Email { get; set; }
}