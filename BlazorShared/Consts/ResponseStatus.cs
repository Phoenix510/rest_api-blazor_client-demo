namespace BlazorShared.Consts;

public static class ResponseStatus
{
    public const string Fail = "Fail";
    public const string Error = "Error";
    public const string Success = "Success";
}