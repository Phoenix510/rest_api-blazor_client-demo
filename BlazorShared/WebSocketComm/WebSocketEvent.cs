namespace BlazorShared.WebSocketComm;

public class WebSocketEvent<T>
{
    public string EventName { get; set; }
    public string DataClassName { get; set; }
    public T Data { get; set; }

    public WebSocketEvent()
    {
    }

    public WebSocketEvent(string eventName, string dataClassName, T data)
    {
        EventName = eventName;
        DataClassName = dataClassName;
        Data = data;
    }
}