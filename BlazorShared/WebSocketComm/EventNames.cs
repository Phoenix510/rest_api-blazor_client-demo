namespace BlazorShared.WebSocketComm;

public static class EventNames
{
    public const string MESSAGE_EVENT = "MessageEvent";
}