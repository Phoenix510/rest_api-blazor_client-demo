using BlazorShared.Contracts.V1;

namespace BlazorShared.WebSocketComm;

public enum MessageType
{
    Sent,
    Deleted
}

public class MessageEvent<T> : WebSocketEvent<T>
{
    public MessageType EventType { get; set; }

    public MessageEvent() : base()
    {
    }

    public MessageEvent(T data, MessageType eventType, string dataClassName, string eventName = EventNames.MESSAGE_EVENT) 
        : base(eventName, dataClassName, data)
    {
        EventType = eventType;
    }
}