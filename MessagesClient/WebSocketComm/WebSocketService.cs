using System.Data;
using System.Net.WebSockets;
using System.Text;
using Blazored.SessionStorage;
using BlazorShared.Contracts.V1;
using BlazorShared.WebSocketComm;
using MessagesClient.Authentication;
using MessagesClient.StartupSettings;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MessagesClient.Services;

public class WebSocketService : IDisposable
{
    private readonly ISessionStorageService sessionStorage;
    private readonly IEventHandler<MessageEvent<JObject>> messageEventHandler;
    private readonly AppOptions options;
    private ClientWebSocket client;

    public WebSocketService(ISessionStorageService sessionStorage, 
        IEventHandler<MessageEvent<JObject>> messageEventHandler,
        IOptions<AppOptions> options)
    {
        this.sessionStorage = sessionStorage;
        this.messageEventHandler = messageEventHandler;
        this.options = options.Value;
        _ = Init();
    }

    async Task Init()
    {
        var token = await sessionStorage.GetItemAsync<string?>(SessionStorageItems.AccessToken);
        _ = Connect(token);
    }

    public async Task Disconnect()
    {
        Console.WriteLine("Disconnecting");
        await client.CloseAsync(WebSocketCloseStatus.NormalClosure, string.Empty, CancellationToken.None);
        client.Dispose();
        client = null;
    }

    public async Task Connect(string token = "")
    {
        try
        {
            client ??= new();

            Console.WriteLine($"Connecting WS! {token}");
            var baseUri = options.SERVER_URI?.Replace("https", "wss");
            if (string.IsNullOrEmpty(baseUri))
                throw new NullReferenceException("Server uri is not set!");
            
            Uri uri = string.IsNullOrEmpty(token)
                ? new Uri($"{baseUri}/{ApiRoutes.MessagesWSController.EstablishConnection}")
                : new Uri($"{baseUri}/{ApiRoutes.MessagesWSController.EstablishConnection}/?token={token}");

            await client.ConnectAsync(
                uri,
                CancellationToken.None);

            var receiveTask = Task.Run(async () =>
            {
                var buffer = new byte[1024];
                while (true)
                {
                    string message = string.Empty;
                    var result = await client.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                    message += Encoding.UTF8.GetString(buffer, 0, result.Count);

                    if (result.MessageType == WebSocketMessageType.Close)
                    {
                        Console.WriteLine("Socket close requested");
                        await Disconnect();
                        break;
                    }

                    if (result.EndOfMessage)
                        ProcessMessage(message);
                }
            });

            await receiveTask;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    private void ProcessMessage(string message)
    {
        dynamic obj = JsonConvert.DeserializeObject<object>(message);

        switch (obj?.EventName.ToString())
        {
            case EventNames.MESSAGE_EVENT:
                messageEventHandler.ProcessEvent(obj.ToObject<MessageEvent<JObject>>());
                break;
            default:
                Console.WriteLine("Invalid web socket message!");
                break;
        }
    }

    public void Dispose()
    {
        Console.WriteLine("Disposing WS connection");
        client.Dispose();
    }
}