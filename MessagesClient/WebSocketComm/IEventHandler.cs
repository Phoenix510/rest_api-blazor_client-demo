namespace MessagesClient.Services;

public interface IEventHandler<in T>
{
    void ProcessEvent(T eventObject);
}