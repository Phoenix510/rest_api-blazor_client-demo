using BlazorShared.Contracts.V1;

namespace MessagesClient.Services;

public class EventBus
{
    public event Action<PublicMessageDTO> NewPublicMessage;
    public event Action<DirectMessageDTO> NewDirectMessage;

    public void NewMessage<T>(T newMessage) where T: MessageDTO
    {
        Console.WriteLine(newMessage.GetType().Name);
        if(newMessage is PublicMessageDTO publicMessage)
            NewPublicMessage?.Invoke(publicMessage);
        else if (newMessage is DirectMessageDTO directMessage)
            NewDirectMessage?.Invoke(directMessage);
        else
            throw new ArgumentOutOfRangeException($"Type {typeof(T).Name} is not supported!");
    }
}