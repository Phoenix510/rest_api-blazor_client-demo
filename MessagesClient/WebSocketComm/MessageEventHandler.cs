using BlazorShared.Contracts.V1;
using BlazorShared.WebSocketComm;
using Newtonsoft.Json.Linq;

namespace MessagesClient.Services;

public class MessageEventHandler : IEventHandler<MessageEvent<JObject>>
{
    private readonly EventBus eventBus;

    public MessageEventHandler(EventBus eventBus)
    {
        this.eventBus = eventBus;
    }

    public void ProcessEvent(MessageEvent<JObject> eventObject)
    {
        switch (eventObject.EventType)
        {
            case MessageType.Sent:
                if (eventObject.DataClassName == nameof(PublicMessageDTO))
                    eventBus.NewMessage(eventObject.Data.ToObject<PublicMessageDTO>());
                else if (eventObject.DataClassName == nameof(DirectMessageDTO))
                    eventBus.NewMessage(eventObject.Data.ToObject<DirectMessageDTO>());
                break;
            case MessageType.Deleted:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}