using System.ComponentModel.DataAnnotations;

namespace MessagesClient.StartupSettings;

public class AppOptions
{
    public const string Name = "AppOptions";

    public string SERVER_URI { get; init; }
}
