﻿using Microsoft.Extensions.Options;

namespace MessagesClient.StartupSettings;

public static class OptionsBuilderExtensions
{
    public static OptionsBuilder<AppOptions> ValidateOptions(this OptionsBuilder<AppOptions> options)
    {
        return options.Validate(opt =>
        {
            if (string.IsNullOrEmpty(opt.SERVER_URI))
                return false;
            else
                return true;
        });
    }
}