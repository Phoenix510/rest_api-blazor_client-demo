using BlazorShared.Contracts.V1;
using MessagesClient.External;

namespace MessagesClient.Services;

public class DirectMessageService
{
    private readonly Client apiClient;
    private readonly AlertService alertService;

    public DirectMessageService(Client apiClient, AlertService alertService)
    {
        this.apiClient = apiClient;
        this.alertService = alertService;
    }

    public async Task<DirectMessageDTO> SendAMessage(CreateDirectMessageModel createMessageModel)
    {
        return await new ErrorHandler<DirectMessageDTO>(alertService)
            .HandleErrorIfNecessary(async () => (await apiClient.InsertAsync(createMessageModel)).Data);
    }
}