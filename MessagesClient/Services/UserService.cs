using Blazored.SessionStorage;
using BlazorShared.Contracts.V1;
using BlazorShared.Contracts.V1.DTO;
using MessagesClient.External;

namespace MessagesClient.Services;

public class UserService
{
    private readonly Client apiClient;
    private readonly AlertService alertService;

    public UserService(Client apiClient, AlertService alertService)
    {
        this.apiClient = apiClient;
        this.alertService = alertService;
    }

    public async Task<(bool success, string message)> SingUp(CreateUserModel registerModel)
    {
        try
        {
            var result = await apiClient.SingUpAsync(registerModel);
            return (true, result.Data);
        }
        catch (ApiException<FailResponse<object>> failResp)
        {
            return (false, $"{failResp.Result.Detail}: {string.Join(", ", failResp.Result.Data)}");
        }
        catch(ApiException ex)
        {
            return (false, ex.Message);
        }
    }

    public async Task<UserDTO> GetDetails()
    {
        return await new ErrorHandler<UserDTO>(alertService)
            .HandleErrorIfNecessary(async () => (await apiClient.GetUserDetailsAsync()).Data);
    }
    
    public async Task<IEnumerable<SearchByUsernameRecord>> GetUsersStartingWith(SerachUserByUsernameModel serachUserByUsernameModel)
    {
        return await new ErrorHandler<IEnumerable<SearchByUsernameRecord>>(alertService)
            .HandleErrorIfNecessary(async () => (await apiClient.GetUsersWhichUsernameStartsWithAsync(serachUserByUsernameModel)).Data);
    }
}