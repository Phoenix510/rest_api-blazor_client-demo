using BlazorShared.Contracts.V1;
using MessagesClient.External;

namespace MessagesClient.Services;

public class ErrorHandler<T>
{
    private readonly AlertService alertService;

    public ErrorHandler(AlertService alertService)
    {
        this.alertService = alertService;
    }

    public async Task<T> HandleErrorIfNecessary(Func<Task<T>> toExecute)
    {
        try
        {
            return await toExecute();
        }
        catch (ApiException<FailResponse<T>> failResp)
        {
            alertService.ShowAlert(failResp.Result.Detail, AlertType.Error);
        }
        catch(ApiException ex)
        {
            alertService.ShowAlert(ex.Message, AlertType.Error);
        }

        return default(T);
    }
}