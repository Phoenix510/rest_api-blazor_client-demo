namespace MessagesClient.Services;

public enum AlertType
{
    Error,
    Info,
    Success
}
public class AlertService
{
    public event Action<string, AlertType> ShowAlertEvent;
    public event Action<AlertType> HideAlertEvent;
    
    public void ShowAlert(string message, AlertType alertType)
    {
        ShowAlertEvent?.Invoke(message, alertType);
    }
    
    public void HideAlert(AlertType alertType)
    {
        HideAlertEvent?.Invoke(alertType);
    }
    
    public void HideAllAlerts()
    {
        foreach (AlertType alertType in (AlertType[]) Enum.GetValues(typeof(AlertType)))
        {
            HideAlert(alertType);
        }
    }
}