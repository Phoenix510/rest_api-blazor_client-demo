using BlazorShared.Contracts.V1;
using BlazorShared.Contracts.V1.DTO;
using MessagesClient.External;

namespace MessagesClient.Services;

public class PublicMessageService
{
    private readonly Client apiClient;
    private readonly AlertService alertService;

    public PublicMessageService(Client apiClient, AlertService alertService)
    {
        this.apiClient = apiClient;
        this.alertService = alertService;
    }

    public async Task<IEnumerable<PublicMessageDTO>> GetPublicMessages(int pageNumber, int pageSize)
    {
        return await new ErrorHandler<IEnumerable<PublicMessageDTO>>(alertService)
            .HandleErrorIfNecessary(async () => (await apiClient.GetAllAsync(pageSize, pageNumber)).Data);
    }

    public async Task<PublicMessageDTO> SendAMessage(CreatePublicMessageModel createMessageModel)
    {
        return await new ErrorHandler<PublicMessageDTO>(alertService)
            .HandleErrorIfNecessary(async () => (await apiClient.Insert2Async(createMessageModel)).Data);
    }
}