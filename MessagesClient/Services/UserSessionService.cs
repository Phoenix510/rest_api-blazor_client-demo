﻿using BlazorShared.Contracts.V1.DTO;
using MessagesClient.External;

namespace MessagesClient.Services;

public class UserSessionService
{
    private readonly Client apiClient;
    private readonly AlertService alertService;

    public UserSessionService(Client apiClient, AlertService alertService)
    {
        this.apiClient = apiClient;
        this.alertService = alertService;
    }

    public async Task<IEnumerable<UserSessionDTO>> GetSessions(int pageSize, int pageNumber)
    {
        return await new ErrorHandler<IEnumerable<UserSessionDTO>>(alertService)
            .HandleErrorIfNecessary(async () => (await apiClient.GetUserSessionsAsync(pageSize, pageNumber)).Data);
    }

    public async Task DeleteSession(string token)
    {
        await new ErrorHandler<object>(alertService)
        .HandleErrorIfNecessary(async () => (await apiClient.DeleteSessionAsync(token)).Data);
    }
}
