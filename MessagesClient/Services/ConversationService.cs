using BlazorShared.Contracts.V1;
using BlazorShared.Contracts.V1.DTO;
using MessagesClient.External;

namespace MessagesClient.Services;

public class ConversationService
{
    private readonly Client apiClient;
    private readonly AlertService alertService;

    public ConversationService(Client apiClient, AlertService alertService)
    {
        this.apiClient = apiClient;
        this.alertService = alertService;
    }
    
    public async Task<IEnumerable<ConversationDTO>> GetLastConversations()
    {
        return await new ErrorHandler<IEnumerable<ConversationDTO>>(alertService)
            .HandleErrorIfNecessary(async () => (await apiClient.GetLastConversationsAsync()).Data);
    }
    
    public async Task<ConversationDTO> CreateConversation(CreateConversationModel createConversationModel)
    {
        return await new ErrorHandler<ConversationDTO>(alertService)
            .HandleErrorIfNecessary(async () => (await apiClient.CreateNewConversationAsync(createConversationModel)).Data);
    }
    
    public async Task<ConversationDTO> GetConversation(long id)
    {
        return await new ErrorHandler<ConversationDTO>(alertService)
            .HandleErrorIfNecessary(async () => (await apiClient.GetConversationDetailsAsync(id)).Data);
    }
    
    public async Task<IEnumerable<DirectMessageDTO>> GetMessagesForConversation(long id, int pageSize, int pageNumber)
    {
        return await new ErrorHandler<IEnumerable<DirectMessageDTO>>(alertService)
            .HandleErrorIfNecessary(async () => (await apiClient.GetMessagesFromConversationAsync(id, pageSize, pageNumber)).Data);
    }
}