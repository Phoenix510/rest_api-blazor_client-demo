using System.Net;
using System.Net.Http.Headers;
using Blazored.SessionStorage;
using MessagesClient.Authentication;

namespace MessagesClient.External;

//https://stackoverflow.com/questions/74333893/nswag-c-sharp-client-doesnt-handle-401-challenge-by-trigger-a-refresh-of-a-to
public partial class CustomDelegatingHandler : DelegatingHandler
{
    private readonly ISessionStorageService sessionStorage;
    private readonly AuthService authService;

    public CustomDelegatingHandler(ISessionStorageService sessionStorageService, AuthService authService): base(new HttpClientHandler())
    {
        sessionStorage = sessionStorageService;
        this.authService = authService;
    }

    private static SemaphoreSlim sem = new SemaphoreSlim(1);

    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
    {
        var response = await base.SendAsync(request, cancellationToken);
        
        if (response.StatusCode == HttpStatusCode.Unauthorized &&
            !string.IsNullOrEmpty(request.Headers.Authorization?.Parameter))
        {
            var expires = await sessionStorage.GetItemAsync<DateTime?>(SessionStorageItems.TokenExpires);
            var refreshToken  = await sessionStorage.GetItemAsync<string?>(SessionStorageItems.RefreshToken);
            
            if (string.IsNullOrWhiteSpace(refreshToken) || expires is null || expires < DateTime.UtcNow)
            {
                await authService.Logout();
                return response;
            }

            await sem.WaitAsync();
            int resp = 400;
            try
            {
                resp = await authService.RefreshToken(refreshToken);
            }
            catch
            {
                resp = 400;
            }
            finally
            {
                sem.Release();
            }

            if (resp != 200)
                return response;

            var accessToken = await sessionStorage.GetItemAsync<string?>(SessionStorageItems.AccessToken);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            response = await base.SendAsync(request, cancellationToken);
        }
        
        return response;
    }
}