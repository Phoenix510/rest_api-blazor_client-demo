using System.Net.Http.Headers;
using Blazored.SessionStorage;
using MessagesClient.Authentication;
using MessagesClient.Services;

namespace MessagesClient.External;

public partial class Client
{
    public void SetJwtToken(string token)
    {
        _httpClient.DefaultRequestHeaders.Clear();
        _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
    }
    
    public void ResetToken()
    {
        _httpClient.DefaultRequestHeaders.Clear();
    }

    public void InitHttpClient(ISessionStorageService sessionStorage, WebSocketService webSocketService)
    {
        _httpClient = new HttpClient(new CustomDelegatingHandler(sessionStorage, new AuthService(this, 
            new AuthStateProvider(this, sessionStorage), sessionStorage, webSocketService)));
    }

}