using System.Security.Claims;
using System.Text;

namespace MessagesClient.Authentication;

public class JwtParser
{
     public static IEnumerable<Claim> ParseClaimsFromJWT(string jwt)
        {
            var claims = new List<Claim>();
            var payload = jwt.Split(".")[1];

            var jsonbytes = ParseBase64WithoutPadding(payload);
            var keyValuePairs = jsonToDict(Encoding.UTF8.GetString(jsonbytes));

            ExtractRolesFromJwt(claims, keyValuePairs);
            claims.AddRange(keyValuePairs.Select(kv => new Claim(kv.Key, kv.Value.ToString())));
            return claims;
        }

        private static Dictionary<string, string> jsonToDict(string json)
        {
            var newDict = new Dictionary<string, string>();
            var values = json.ToString().Trim().TrimStart('{')
                  .TrimEnd('}').Split(',');

            for (int i = 0; i < values.Length; i++)
            {
                var thisValue = values[i];

                while (values[i].Contains('[') && !values[i].Contains(']'))
                {
                    i++;
                    thisValue += ',' + values[i];
                }

                var itemValues = thisValue.Split(new string[] { "\":" }, StringSplitOptions.None);
                Console.WriteLine(itemValues.Length);
                itemValues[0] = itemValues[0].Trim('"');

                itemValues[1] = itemValues[1].Trim('"');
                newDict.Add(itemValues[0], itemValues[1]);
            }

            return newDict;
        }

        private static void ExtractRolesFromJwt(List<Claim> claims, Dictionary<string, string> keyValuesPairs)
        {
            keyValuesPairs.TryGetValue(ClaimTypes.Role, out string roles);

            if (roles is not null)
            {
                var parsedRoles = roles.Trim().TrimStart('[')
                    .TrimEnd(']').Split(',');

                if (parsedRoles.Length > 1)
                {
                    foreach (var role in parsedRoles)
                    {
                        claims.Add(new Claim(ClaimTypes.Role, role.Trim('"')));
                    }
                }
                else
                {
                    claims.Add(new Claim(ClaimTypes.Role, parsedRoles[0]));
                }

                keyValuesPairs.Remove(ClaimTypes.Role);
            }
        }

        private static byte[] ParseBase64WithoutPadding(string base64)
        {
            switch (base64.Length % 4)
            {
                case 2:
                    base64 += "==";
                    break;
                case 3:
                    base64 += "=";
                    break;
            }
            return Convert.FromBase64String(base64);
        }
}