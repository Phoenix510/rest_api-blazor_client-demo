using Blazored.SessionStorage;
using BlazorShared.Consts;
using BlazorShared.Contracts.V1;
using MessagesClient.External;
using MessagesClient.Services;
using Microsoft.AspNetCore.Components.Authorization;
using Client = MessagesClient.External.Client;

namespace MessagesClient.Authentication;

public static class SessionStorageItems
{
    public const string AccessToken = "authToken";
    public const string RefreshToken = "refreshToken";
    public const string TokenExpires = "tokenExpires";
}

public class AuthService
{
    private readonly Client client;
    private readonly AuthenticationStateProvider authenticationStateProvider;
    private readonly ISessionStorageService sessionStorage;
    private readonly WebSocketService webSocketService;

    public AuthService(Client client, AuthenticationStateProvider authenticationStateProvider,
        ISessionStorageService sessionStorage, WebSocketService webSocketService)
    {
        this.client = client;
        this.authenticationStateProvider = authenticationStateProvider;
        this.sessionStorage = sessionStorage;
        this.webSocketService = webSocketService;
    }

    public async Task<(int statusCode, FailResponse<object> response)> Login(LoginModel credentials)
    {
        try
        {
            var response = await client.LoginAsync(credentials);
            if (response.ResponseStatus == ResponseStatus.Success)
                await SetTokenData(response.Data);
            
            await RefreshWSSession(response.Data.AccessToken);
            
            return (200, null);
        }
        catch (ApiException<FailResponse<object>> ex)
        {
            return (ex.StatusCode, ex.Result);
        }
        catch (ApiException ex)
        {
            return (ex.StatusCode, null);
        }
    }

    public async Task Logout()
    {
        var refreshToken = await sessionStorage.GetItemAsStringAsync(SessionStorageItems.RefreshToken);
        await sessionStorage.RemoveItemAsync(SessionStorageItems.AccessToken);
        await sessionStorage.RemoveItemAsync(SessionStorageItems.RefreshToken);

        await RefreshWSSession(string.Empty);
        client.ResetToken();

        ((AuthStateProvider)authenticationStateProvider).NotifyUserLogout();
    }
    
    public async Task<int> SingUp(CreateUserModel createUserModel)
    {
        try
        {
            await client.SingUpAsync(createUserModel);
            return 200;
        }
        catch (ApiException ex)
        {
            return ex.StatusCode;
        }
    }

    public async Task<int> RefreshToken(string refreshToken)
    {
        try
        {
            var result = await client.RefreshSessionAsync(refreshToken);
            if (result?.ResponseStatus == ResponseStatus.Success)
                await SetTokenData(result.Data);
            else
                return 400;

            return 200;
        }
        catch (ApiException ex)
        {
            return ex.StatusCode;
        }
    }
    
    private async Task SetTokenData(TokenModel tokenModel)
    {
        await sessionStorage.SetItemAsStringAsync(SessionStorageItems.AccessToken, tokenModel.AccessToken);
        await sessionStorage.SetItemAsStringAsync(SessionStorageItems.RefreshToken, tokenModel.RefreshToken);
        await sessionStorage.SetItemAsync<DateTime>(SessionStorageItems.TokenExpires, DateTime.Now.AddMinutes(16));
        ((AuthStateProvider)authenticationStateProvider).NotifyUserAuthentication(tokenModel.AccessToken);

        client.SetJwtToken(tokenModel.AccessToken);
    }

    private async Task RefreshWSSession(string token)
    {
        await webSocketService.Disconnect();
        await webSocketService.Connect(token);
    }
}