using Blazored.LocalStorage;
using Blazored.SessionStorage;
using BlazorShared.WebSocketComm;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using MessagesClient;
using MessagesClient.Authentication;
using MessagesClient.External;
using MessagesClient.Services;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using MessagesClient.StartupSettings;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddOptions<AppOptions>()
    .Bind(builder.Configuration.GetSection(AppOptions.Name))
    .ValidateOptions()
    .ValidateOnStart();

builder.Services.AddBlazoredLocalStorage();
builder.Services.AddBlazoredSessionStorageAsSingleton();
builder.Services.AddSingleton<EventBus>();
builder.Services.AddSingleton<IEventHandler<MessageEvent<JObject>>, MessageEventHandler>();
builder.Services.AddSingleton<WebSocketService>();


builder.Services.AddScoped(serviceProvider =>
{
    var options = serviceProvider.GetService<IOptions<AppOptions>>().Value;
    var client = new Client(options.SERVER_URI, new());
    var sessionStorage = serviceProvider.GetService<ISessionStorageService>();
    var webSocketService = serviceProvider.GetService<WebSocketService>();

    client.InitHttpClient(sessionStorage, webSocketService);
    return client;
});

builder.Services.AddScoped<AlertService>();
builder.Services.AddScoped<AuthService>();
builder.Services.AddAuthorizationCore();
builder.Services.AddScoped<AuthenticationStateProvider, AuthStateProvider>();
builder.Services.AddScoped<UserService>();
builder.Services.AddScoped<PublicMessageService>();
builder.Services.AddScoped<DirectMessageService>();
builder.Services.AddScoped<ConversationService>();
builder.Services.AddScoped<UserSessionService>();

await builder.Build().RunAsync();