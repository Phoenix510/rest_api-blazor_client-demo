﻿using ApplicationCore.Extensions;
using System.Security.Claims;

namespace ApplicationCore.Tests.ExtensionsTests
{
    public class ClassPrincipalsExtensionsTests
    {
        [Fact]
        public void Valid_username_is_returned()
        {
            var username = "test_username";
            var sut = new ClaimsPrincipal(
                new List<ClaimsIdentity> {
                    new ClaimsIdentity(new List<Claim>() { new(ClaimTypes.Name, username) })
                    });

            var result = sut.GetUsername();
            Assert.Equal(username, result);
        }

        [Fact]
        public void When_username_is_not_present_empty_string_is_returned()
        {
            var sut = new ClaimsPrincipal();

            var result = sut.GetUsername();
            Assert.Equal(string.Empty, result);
        }
    }
}
