﻿using ApplicationCore.Extensions;

namespace ApplicationCore.Tests.ExtensionsTests
{
    public class UserRepositoryExtensionTests
    {
        [Fact]
        public void Valid_user_is_returned_by_username()
        {
            var sut = GetRepositoryMock();
            sut.Query.Returns(new List<User>()
            {
                new User() { Username = "test" },
                new User() { Username = "test2"}
            }.AsQueryable());
            
            var result = sut.GetByUsername("test");

            Assert.NotNull(result);
            Assert.Equal("test", result.Username);
        }

        [Fact]
        public void When_user_is_not_found_by_username_null_is_returned()
        {
            var sut = GetRepositoryMock();
            sut.Query.Returns(new List<User>()
            {
                new User() { Username = "test2"}
            }.AsQueryable());

            var result = sut.GetByUsername("test");

            Assert.Null(result);
        }

        [Fact]
        public void Valid_user_is_returned_by_email()
        {
            var sut = GetRepositoryMock();
            sut.Query.Returns(new List<User>()
            {
                new User() { Email = "test@example" },
                new User() { Email = "test2@example"}
            }.AsQueryable());

            var result = sut.GetByEmail("test@example");

            Assert.NotNull(result);
            Assert.Equal("test@example", result.Email);
        }

        // I haven't found yet how to test it, as it's only insensitive when working with queries made on database
        //[Fact]
        //public void Letter_case_is_ignored_when_user_is_searched_by_email()
        //{
        //    var sut = GetRepositoryMock();
        //    sut.Query.Returns(new List<User>()
        //    {
        //        new User() { Email = "test@example" },
        //        new User() { Email = "test2@example"}
        //    }.AsQueryable());

        //    var result = sut.GetByEmail("Test@exAmple");

        //    Assert.NotNull(result);
        //    Assert.Equal("test@example", result.Email);
        //}

        [Fact]
        public void When_user_is_not_found_by_email_null_is_returned()
        {
            var sut = GetRepositoryMock();
            sut.Query.Returns(new List<User>()
            {
                new User() { Email = "test2@example"}
            }.AsQueryable());

            var result = sut.GetByEmail("test@example");

            Assert.Null(result);
        }

        IRepository<User> GetRepositoryMock()
            => Substitute.For<IRepository<User>>();
    }
}
