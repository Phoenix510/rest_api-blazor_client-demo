﻿namespace ApplicationCore.Tests.AsyncQueryProvider
{
    public static class IEnumExtensions
    {
        public static AsyncEnumerable<T> AsAsyncQueryable<T>(this IEnumerable<T> enumerable)
            => new AsyncEnumerable<T>(enumerable);
    }
}
