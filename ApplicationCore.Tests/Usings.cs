global using Xunit;
global using BlazorShared.Contracts.V1.DTO;
global using ApplicationCore.Entities;
global using ApplicationCore.Interfaces;
global using NSubstitute;
global using ApplicationCore.Services;