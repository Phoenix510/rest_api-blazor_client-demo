﻿using ApplicationCore.JWT;
using ApplicationCore.Services;
using BlazorShared.Converters;
using NSubstitute;
using System.Net;
using System.Security.Claims;
using BC = BCrypt.Net;

namespace ApplicationCore.Tests.ServicesTests
{
    public class AuthServiceTest
    {
        [Fact]
        public async Task User_is_authenticated_when_correct_credentials_are_provided()
        {
            var userSessionMock = GetUserSessionMock();
            var sut = new AuthenticationService(GetRepositoryMock(), GetJWTManagerMock(), 
                userSessionMock, Substitute.For<IRepository<Continent>>(), Substitute.For<IRepository<Country>>(),
                GetIPDetailResolverMock(), GetIPConverterTests());

            (bool valid, BlazorShared.Contracts.V1.TokenModel token) result = await sut.AuthUser("test", "test", IPAddress.Parse("1.1.1.1"));

            Assert.True(result.valid);
            Assert.NotNull(result.token);
            Assert.Equal("test_acc_token", result.token.AccessToken);
            Assert.Equal("test_ref_token", result.token.RefreshToken);
            userSessionMock.Received(1).Insert(Arg.Any<UserSession>());
            await userSessionMock.Received(1).SaveChangesAsync();
        }

        [Fact]
        public async Task User_is_not_authenticated_when_username_is_invalid()
        {
            var userSessionMock = GetUserSessionMock();
            var sut = new AuthenticationService(GetRepositoryMock(), GetJWTManagerMock(),
                          userSessionMock, Substitute.For<IRepository<Continent>>(), Substitute.For<IRepository<Country>>(),
                          GetIPDetailResolverMock(), GetIPConverterTests());

            (bool valid, BlazorShared.Contracts.V1.TokenModel token) result = await sut.AuthUser("invalid", "test", IPAddress.Parse("1.1.1.1"));

            Assert.False(result.valid);
            Assert.Null(result.token);
            userSessionMock.DidNotReceive().Insert(Arg.Any<UserSession>());
            await userSessionMock.DidNotReceive().SaveChangesAsync();
        }

        [Fact]
        public async Task User_is_not_authenticated_when_password_is_invalid()
        {
            var userSessionMock = GetUserSessionMock();
            var sut = new AuthenticationService(GetRepositoryMock(), GetJWTManagerMock(),
                          userSessionMock, Substitute.For<IRepository<Continent>>(), Substitute.For<IRepository<Country>>(),
                          GetIPDetailResolverMock(), GetIPConverterTests());

            (bool valid, BlazorShared.Contracts.V1.TokenModel token) result = await sut.AuthUser("test", "invalid", IPAddress.Parse("1.1.1.1"));

            Assert.False(result.valid);
            Assert.Null(result.token);
            userSessionMock.DidNotReceive().Insert(Arg.Any<UserSession>());
            await userSessionMock.DidNotReceive().SaveChangesAsync();
        }

        [Fact]
        public async Task Refresh_token_is_generated_when_session_exist_and_token_type_is_valid()
        {
            var userSessionMock = GetUserSessionMock();
            var sut = new AuthenticationService(GetRepositoryMock(), GetJWTManagerMock(),
                          userSessionMock, Substitute.For<IRepository<Continent>>(), Substitute.For<IRepository<Country>>(),
                          GetIPDetailResolverMock(), GetIPConverterTests());

            (bool valid, BlazorShared.Contracts.V1.TokenModel token) result = await sut.RefreshSession("test_ref_token", IPAddress.Parse("1.1.1.1"));

            Assert.True(result.valid);
            Assert.NotNull(result.token);
            Assert.Equal("test_acc_token", result.token.AccessToken);
            Assert.Equal("test_ref_token", result.token.RefreshToken);
            userSessionMock.Received(1).Insert(Arg.Any<UserSession>());
            await userSessionMock.Received(1).SaveChangesAsync();
        }

        [Fact]
        public async Task Refresh_token_is_not_generated_when_refresh_token_is_invalid()
        {
            var userSessionMock = GetUserSessionMock();
            var sut = new AuthenticationService(GetRepositoryMock(), GetJWTManagerMock(),
                          userSessionMock, Substitute.For<IRepository<Continent>>(), Substitute.For<IRepository<Country>>(),
                          GetIPDetailResolverMock(), GetIPConverterTests());

            (bool valid, BlazorShared.Contracts.V1.TokenModel token) result = await sut.RefreshSession("invalid_test_ref_token", IPAddress.Parse("1.1.1.1"));

            Assert.False(result.valid);
            Assert.Null(result.token);
            userSessionMock.DidNotReceive().Insert(Arg.Any<UserSession>());
            await userSessionMock.DidNotReceive().SaveChangesAsync();
        }

        [Fact]
        public async Task Refresh_token_is_not_generated_when_token_type_is_invalid()
        {
            var userSessionMock = GetUserSessionMock();
            var sut = new AuthenticationService(GetRepositoryMock(), GetJWTManagerMock(),
                          userSessionMock, Substitute.For<IRepository<Continent>>(), Substitute.For<IRepository<Country>>(),
                          GetIPDetailResolverMock(), GetIPConverterTests());

            (bool valid, BlazorShared.Contracts.V1.TokenModel token) result = await sut.RefreshSession("test_cookie_token", IPAddress.Parse("1.1.1.1"));

            Assert.False(result.valid);
            Assert.Null(result.token);
            userSessionMock.DidNotReceive().Insert(Arg.Any<UserSession>());
            await userSessionMock.DidNotReceive().SaveChangesAsync();
        }


        private IRepository<User> GetRepositoryMock()
        {
            var mock = Substitute.For<IRepository<User>>();
            mock.Query.Returns(new List<User>()
            {
                new User() { Username = "test23", Id =1},
                new User() { Username = "test", Id=2, Password=BC.BCrypt.HashPassword("test")}
            }.AsQueryable());

            return mock;
        }

        private IJWTManager GetJWTManagerMock()
        {
            var mock = Substitute.For<IJWTManager>();
            mock.GenerateAccessToken(Arg.Any<string>()).Returns("test_acc_token");
            mock.GenerateRefreshToken(default).ReturnsForAnyArgs("test_ref_token");

            return mock;
        }

        private IRepository<UserSession> GetUserSessionMock()
        {
            var mock = Substitute.For<IRepository<UserSession>>();
            mock.Query.Returns(new List<UserSession>()
            {
                new UserSession() { UserId = 1, Token = "test_ref_token", TokenType=JWTManager.REFRESH_TOKEN},
                new UserSession() { UserId = 1, Token = "test_cookie_token", TokenType=JWTManager.DEVICE_COOKIE},
            }.AsQueryable());

            return mock;
        }

        private IIPDetailsResolver GetIPDetailResolverMock()
        {
            var mock = Substitute.For<IIPDetailsResolver>();
            mock.GetIPDetails(default).ReturnsForAnyArgs(new IpDetails() { City = "test" });

            return mock;
        }

        private IIPConverter GetIPConverterTests()
        {
            var mock = Substitute.For<IIPConverter>();

            return mock;
        }
    }
}
