﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System.Net;

namespace ApplicationCore.Tests.ServicesTests
{
    public class IPDetailsResolverTests
    {
        [Fact]
        public async Task IPDetails_are_returned()
        {
            var sut = new IPDetailsResolver(GetGeolocationMock());

            var result = await sut.GetIPDetails(IPAddress.Parse("1.1.1.1"));

            Assert.NotNull(result);
            Assert.Equal("test", result.City);
        }

        public IGeolocationService GetGeolocationMock()
        {
            var mock = Substitute.For<IGeolocationService>();
            mock.GetIpDetails(default).ReturnsForAnyArgs(new IpDetails() { City = "test" });

            return mock;
        }
    }
}
