﻿using ApplicationCore.Tests.AsyncQueryProvider;

namespace ApplicationCore.Tests.ServicesTests
{
    public class ConversationServiceTests
    {
        [Fact]
        public void If_creator_does_not_exist_conversation_is_not_added()
        {
            var sut = new ConversationService(Substitute.For<IRepository<DirectMessage>>(),
                Substitute.For<IRepository<Conversation>>(),
                GetUserRepositoryMock());
            var createConv = new CreateConversationModel() { ParticipantId = 1, Name = "Example" };
            var creator = new User() { Id = 20 };

            (IEnumerable<string> errors, Conversation conversation) result = sut.Insert(createConv, creator);

            Assert.Single(result.errors);
            Assert.Null(result.conversation);
            Assert.Equal("Creator does not exist!", result.errors.First());
        }

        [Fact]
        public void If_participant_does_not_exist_conversation_is_not_added()
        {
            var sut = new ConversationService(Substitute.For<IRepository<DirectMessage>>(),
                Substitute.For<IRepository<Conversation>>(),
                GetUserRepositoryMock());
            var createConv = new CreateConversationModel() { ParticipantId = 20, Name = "Example" };
            var creator = new User() { Id = 1 };

            (IEnumerable<string> errors, Conversation conversation) result = sut.Insert(createConv, creator);

            Assert.Single(result.errors);
            Assert.Null(result.conversation);
            Assert.Equal("Participant does not exist!", result.errors.First());
        }

        [Fact]
        public void If_conversation_already_exist_new_is_not_added()
        {
            var convMock = Substitute.For<IRepository<Conversation>>();
            convMock.Query.Returns(new Conversation[] {
                new Conversation()
                { Id = 1,
                    Participants = new List<User>()
                    {
                        new User() { Id = 1 },
                        new User() { Id = 2 }
                    }
                }
            }.AsQueryable());

            var sut = new ConversationService(Substitute.For<IRepository<DirectMessage>>(),
                convMock,
                GetUserRepositoryMock());
            var createConv = new CreateConversationModel()
            { ParticipantId = 2, Name = "Example" };
            var creator = new User() { Id = 1 };

            (IEnumerable<string> errors, Conversation conversation) result = sut.Insert(createConv, creator);

            convMock.DidNotReceive().Insert(Arg.Any<Conversation>());
            Assert.Empty(result.errors);
            Assert.NotNull(result.conversation);
            Assert.Equal(1, result.conversation.Id);
        }

        [Fact]
        public void New_conversation_is_added()
        {
            var convMock = Substitute.For<IRepository<Conversation>>();
            convMock.Query.Returns(new Conversation[] {
                new Conversation()
                { Id = 1,
                    Participants = new List<User>()
                    {
                        new User() { Id = 1 },
                        new User() { Id = 4 }
                    }
                }
            }.AsQueryable());

            var sut = new ConversationService(Substitute.For<IRepository<DirectMessage>>(),
                convMock,
                GetUserRepositoryMock());
            var createConv = new CreateConversationModel()
            { ParticipantId = 2, Name = "Example" };
            var creator = new User() { Id = 1 };

            (IEnumerable<string> errors, Conversation conversation) result = sut.Insert(createConv, creator);

            convMock.Received(1).Insert(Arg.Any<Conversation>());
            Assert.Empty(result.errors);
            Assert.NotNull(result.conversation);
        }

        [Fact]
        public async Task If_user_is_not_a_part_of_conversation_messages_are_not_returned()
        {
            var messagesMock = Substitute.For<IRepository<DirectMessage>>();
            messagesMock.Query.Returns(new DirectMessage[] {
                new DirectMessage()
                { Id = 1,
                  ConversationId =1,
                   Content = "OK"
                }
            }.AsQueryable());

            var sut = new ConversationService(messagesMock,
                GetConvMock(),
                GetUserRepositoryMock());

            var result = await sut.GetMessagesFromConversation(1, 5, 1, 1);

            Assert.Empty(result);
        }

        [Fact]
        public async Task If_conversation_does_not_exist_messages_are_not_returned()
        {
            var messagesMock = Substitute.For<IRepository<DirectMessage>>();
            messagesMock.Query.Returns(new DirectMessage[] {
                new DirectMessage()
                { Id = 1,
                  ConversationId =1,
                   Content = "OK"
                }
            }.AsQueryable());

            var sut = new ConversationService(messagesMock,
                GetConvMock(),
                GetUserRepositoryMock());

            var result = await sut.GetMessagesFromConversation(5, 1, 1, 1);

            Assert.Empty(result);
        }

        [Fact]
        public async Task Only_messages_for_conversation_are_returned()
        {
            var messagesMock = Substitute.For<IRepository<DirectMessage>>();
            messagesMock.Query.Returns(new DirectMessage[] {
                new DirectMessage()
                { Id = 1,
                ConversationId =1,
                   Content = "OK"
                },
                new DirectMessage()
                { Id = 1,
                ConversationId =2,
                   Content = "OK"
                }
            }.AsAsyncQueryable());

            var sut = new ConversationService(messagesMock,
                GetConvMock(),
                GetUserRepositoryMock());

            var result = await sut.GetMessagesFromConversation(1, 1, 1, 1);

            Assert.Single(result);
        }

        [Fact]
        public async Task Valid_message_page_is_returned()
        {
            var messagesMock = Substitute.For<IRepository<DirectMessage>>();
            messagesMock.Query.Returns(new DirectMessage[] {
                new DirectMessage()
                { Id = 1,
                ConversationId =1,
                   Content = "OK",
                     CreationDate = DateTimeOffset.UtcNow
                },
                new DirectMessage()
                { Id = 2,
                ConversationId =1,
                   Content = "OK2",
                   CreationDate = DateTimeOffset.UtcNow.Add(TimeSpan.FromMinutes(20))
                }
            }.AsAsyncQueryable());

            var sut = new ConversationService(messagesMock,
                GetConvMock(),
                GetUserRepositoryMock());

            // first page - second item
            // (because items are returned in descending order by creation date)
            var result = await sut.GetMessagesFromConversation(1, 1, 1, 1);

            Assert.Single(result);
            Assert.Equal(2, result.First().Id);

            // second page - first item
            result = await sut.GetMessagesFromConversation(1, 1, 1, 2);

            Assert.Single(result);
            Assert.Equal(1, result.First().Id);

            // third page - empty
            result = await sut.GetMessagesFromConversation(1, 1, 1, 3);

            Assert.Empty(result);

            // get page of size 2 - 2 items returned
            result = await sut.GetMessagesFromConversation(1, 1, 2, 1);

            Assert.Equal(2, result.Count());
        }

        [Fact]
        public void Placeholder()
        {
            throw new NotImplementedException
                ("To be tested GetConversationDetails, GetLastConversationsWithLastMessage");
        }

        private IRepository<Conversation> GetConvMock()
        {
            var convMock = Substitute.For<IRepository<Conversation>>();
            convMock.Query.Returns(new Conversation[] {
                new Conversation()
                { Id = 1,
                    Participants = new List<User>()
                    {
                        new User() { Id = 1 },
                        new User() { Id = 4 }
                    }
                }
            }.AsQueryable());

            return convMock;
        }


        private IRepository<User> GetUserRepositoryMock()
        {
            var mock = Substitute.For<IRepository<User>>();
            mock.Find(1L).Returns(new User() { Id = 1, Username = "test23" });
            mock.Find(2L).Returns(new User() { Id = 2, Username = "test" });

            return mock;
        }
    }
}
