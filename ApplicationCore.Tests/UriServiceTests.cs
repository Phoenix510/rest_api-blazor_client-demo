using ApplicationCore.Services;
using BlazorShared.Contracts.V1;


namespace ApplicationCore.Tests;

public class UriServiceTests
{
    [Fact]
    public void Direct_messages_uri_is_correctly_generated()
    {
        var sut = new UriService("http://test");
        
        var result = sut.GetMessagesFromConversationUri(1, new PageRequest(1, 2));

        var expected = "http://test/api/v1/Conversation/GetMessagesFromConversation/1?pageNumber=2&pageSize=1";
        Assert.Equal(expected, result.ToString());
    }
    
    [Fact]
    public void Public_messages_uri_is_correctly_generated()
    {
        var sut = new UriService("http://test");
        
        var result = sut.GetAllPublicMessagesUri(new PageRequest(1, 2));

        var expected = $"http://test/{ApiRoutes.PublicMessages.GetAll}?pageNumber=2&pageSize=1";
        Assert.Equal(expected, result.ToString());
    }
}